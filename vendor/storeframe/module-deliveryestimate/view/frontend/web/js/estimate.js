define(
    [
        'jquery',
        'mage/url',
        'underscore'
    ],
    function ($, MageUrl, _) {
        return function (config) {
            var productIds = {'product_ids': config.productIds};
            var url = MageUrl.build('/deliveryestimate/index/estimate');
            getTextDeliveryEstimated(productIds, false);
            $(document).on('swissup:ajaxlayerednavigation:reload:after',function () {
                getTextDeliveryEstimated(productIds, false);
            });


            function getTextDeliveryEstimated(productIds, swatch) {

                $.ajax({
                    url: url,
                    type: "GET",
                    data: productIds,
                    success: function (response) {
                        $('.delivery-estimate-text').each(function () {
                            var productId = $(this).data('product-id');
                            if (response[productId]) {
                                var text = response[productId].text,
                                    isBackorder = response[productId].is_backorder;
                                if (!isBackorder) {
                                    var icon = '<em class="fa fa-lg fa-calendar-check"></em>';
                                } else {
                                    var icon = '<em class="fa fa-lg fa-shipping-timed"></em>';
                                    $('.delivery-estimate-text[data-product-id="' + productId + '"]').css('color', '#777');
                                }

                                if (text !== '') {
                                    if (swatch) {
                                        return $('.delivery-estimate-text').html(icon + ' <span>' + text + '</span>');
                                    }

                                    $('.delivery-estimate-text[data-product-id="' + productId + '"]').html(icon + ' <span>' + text + '</span>');
                                }
                            }
                        });
                    }
                });
            }
        }
    }
);
