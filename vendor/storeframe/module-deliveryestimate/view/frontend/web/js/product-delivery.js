define(
    [
        'jquery',
        'mage/url',
        'underscore'
    ],
    function ($, MageUrl, _) {
        var estimatedIds = [];
        $(function () {
            var url = MageUrl.build('/deliveryestimate/index/index');

            $('.delivery-estimate-text').each(function () {
                var productId = $(this).data('product-id');
                getTextDeliveryEstimated(productId, false);
            });

            function getTextDeliveryEstimated(productId, swatch) {
                if (estimatedIds.indexOf(productId) !== -1) {
                    return;
                }
                estimatedIds.push(productId);

                $.ajax({
                    url: url,
                    type: "GET",
                    data: {product_id: productId},
                    success: function (response) {
                        if (!response.is_backorder) {
                            var icon = '<em class="fa fa-lg fa-calendar-check"></em>';
                            $('.delivery-estimate-text[data-product-id="' + productId + '"]').addClass('in-stock');
                        } else {
                            var icon = '<em class="fa fa-lg fa-shipping-timed"></em>';
                            $('.delivery-estimate-text[data-product-id="' + productId + '"]').addClass('backordered');
                        }

                        if (response.text !== '') {
                            if (swatch) {
                                return $('.delivery-estimate-text').html(icon + ' <span>' + response.text + '</span>');
                            }

                            $('.delivery-estimate-text[data-product-id="' + productId + '"]').html(icon + ' <span>' + response.text + '</span>');
                        }
                    }
                });
            }

            var existCondition = setInterval(function() {
                if ($(".product-options-wrapper .swatch-opt .swatch-option").length) {
                    console.log("Exists!");
                    clearInterval(existCondition);
                    $(".product-options-wrapper .swatch-opt .swatch-option").on('click', function () {
                        setTimeout(function () {
                            selectPro();
                        },500);
                    });
                }
            }, 100); // check every 100ms

            function selectPro() {
                var selectedOptions = {};
                $('#product-options-wrapper div.swatch-attribute').each(function (k, v) {
                    var attributeId = jQuery(v).attr('data-attribute-id');
                    var optionSelected = jQuery(v).attr('data-option-selected');
                    if (!attributeId || !optionSelected) {
                        return;
                    }
                    selectedOptions[attributeId] = optionSelected.toString();
                });

                var productIdIndex = $('[data-role=swatch-options]').data('mageSwatchRenderer').options.jsonConfig.index;
                var foundIds = [];
                $.each(productIdIndex, function (productId, attributes) {
                    var productIsSelected = function (attributes, selectedOptions) {
                        return _.isEqual(attributes, selectedOptions);
                    };
                    if (productIsSelected(attributes, selectedOptions)) {
                        foundIds.push(productId);
                    }
                });

                if (foundIds.length) {
                    estimatedIds = [];
                    getTextDeliveryEstimated(foundIds[0], true);
                }
            }
        });
    });
