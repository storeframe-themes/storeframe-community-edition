<?php

namespace Storeframe\DeliveryEstimate\Controller\Index;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Action\Action;
use Psr\Log\LoggerInterface;

class Index extends Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var \Storeframe\DeliveryEstimate\Helper\Data
     */
    protected $helper;

    /**
     * Index constructor.
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     * @param \Storeframe\DeliveryEstimate\Helper\Data $helper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        LoggerInterface $logger,
        ProductRepositoryInterface $productRepository,
        \Storeframe\DeliveryEstimate\Helper\Data $helper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\App\Action\Context $context
    ) {
        $this->helper = $helper;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();

        $productId = $this->getRequest()->get('product_id');
        try {
            $product = $this->productRepository->getById($productId);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
        $isBackorder = false;
        if (!empty($product)) {
            $text = $this->helper->getTextDeliveryEstimated($product);
            $isBackorder = $this->helper->isBackorder();
        } else {
            $text = '';
        }

        return $resultJson->setData(['text' => $text, 'is_backorder' => $isBackorder]);
    }
}
