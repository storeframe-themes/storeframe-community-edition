<?php
namespace Storeframe\DeliveryEstimate\Controller\Index;

use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Exception\InputException;
use Psr\Log\LoggerInterface;

/**
 * Class Estimate
 *
 * @package Bss\DeliveryEstimate\Controller\Index
 */
class Estimate extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Storeframe\DeliveryEstimate\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Index constructor.
     * @param LoggerInterface $logger
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Storeframe\DeliveryEstimate\Helper\Data $helper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        LoggerInterface $logger,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Storeframe\DeliveryEstimate\Helper\Data $helper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\App\Action\Context $context
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->logger = $logger;
    }

    /**
     * Set data Ajax
     *
     * @return \Magento\Framework\App\ResponseInterface|Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $productIds = $this->getRequest()->get('product_ids');
        $productIds = explode(',', $productIds);
        $data = [];
        try {
            $productsCollection = $this->productCollectionFactory->create();
            $products = $productsCollection->addAttributeToSelect('*')->addAttributeToFilter('entity_id', $productIds);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
        if (!empty($products)) {
            foreach ($products as $product) {
                $text = $this->helper->getTextDeliveryEstimated($product);
                $isBackorder = $this->helper->isBackorder();
                $data[$product->getId()] = [
                        'text' => $text,
                        'is_backorder' => $isBackorder
                ];
            }
        }
        return $resultJson->setData($data);
    }
}