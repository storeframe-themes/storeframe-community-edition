<?php
namespace Storeframe\DeliveryEstimate\Model\Config\Source;

class Weekend implements \Magento\Framework\Option\ArrayInterface
{
    public static $SATURDAY = 6;
    public static $SUNDAY = 0;
    public static $DISABLE = -1;
    /**
     * @return array
     */
    public function toOptionArray()
    {

        return [
            ['value' => static::$DISABLE, 'label' => __('Disable')],
            ['value' => static::$SATURDAY, 'label' => __('Saturday')],
            ['value' => static::$SATURDAY . ',' . static::$SUNDAY, 'label' => __('Saturday & Sunday')],
        ];
    }
}