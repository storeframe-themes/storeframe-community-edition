<?php
namespace Storeframe\DeliveryEstimate\Model\Config\Source;

class Yesno implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 0, 'label' => __('Disable')], ['value' => 1, 'label' => __('Enable')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('Disable'), 1 => __('Enable')];
    }
}