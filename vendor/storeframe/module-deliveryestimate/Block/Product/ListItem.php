<?php

namespace Storeframe\DeliveryEstimate\Block\Product;

class ListItem extends \Magento\Catalog\Block\Product\ProductList\Item\Block
{

    protected $_helper;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        array $data = [],
        \Storeframe\DeliveryEstimate\Helper\Data $_helper
    )
    {
        parent::__construct(
            $context,
            $data
        );
        $this->_helper = $_helper;
    }

    protected function _init()
    {
        $this->_helper->setProduct($this->getProduct());
    }

    /**
     * Return text delivery estimate
     *
     * @return string | null
     */
    public function getTextDeliveryEstimated($product)
    {
        $this->_init();
        return $this->_helper->getTextDeliveryEstimated($productId);
    }

    public function getCurrentProductId()
    {
        $this->_init();
        return $this->_helper->getCurrentProductId();
    }

    public function allowShow()
    {
        return $this->_helper->isAllowShowingProductListPage();
    }

    public function toHtml()
    {
        if(!$this->allowShow()){
            return '';
        }
        return parent::toHtml();
    }
}