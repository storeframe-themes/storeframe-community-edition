<?php

namespace Storeframe\DeliveryEstimate\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{

    /**
     * Customer setup factory
     *
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $_eavSetupFactory;

    /**
     * Init
     *
     * @param \Magento\Eav\Setup\EavSetupFactory
     * CategorySetupFactory categorySetupFactory
     */
    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory) {
        $this->_eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Installs DB schema for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->_eavSetupFactory->create([
            'setup' => $setup
        ]);

        if ($context->getVersion() < '1.0.5') {
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'sf_delivery_estimate',
                [
                    'type' => 'text',
                    'label' => '*Delivery Estimate',
                    'required' => false,
                    'searchable' => false,
                    'filterable' => false,
                    'is_use_define' => true,
                    'class' => '',
                    'group' => '',
                    'input' => 'text',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'visible' => true,
                    'user_defined' => false,
                    'default' => '',
                    'sort_order' => 20,
                    'apply_to' => '',
                    'note' => '',
                ]
            );
        }
	
		if ($context->getVersion() < '1.0.6') {
			$eavSetup->updateAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'sf_delivery_estimate',
				[
					'attribute_code' => 'stf_delivery_estimate'
				]
			);
		}
    }
}