<?php
namespace Storeframe\DeliveryEstimate\Helper;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 *
 * @package Storeframe\DeliveryEstimate\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_date;

    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var
     */
    protected $_product;

    /**
     * @var mixed
     */
    protected $_stockRegistry;

    /**
     * @var bool
     */
    protected $_isBackorder = false;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected $serializer;

    /**
     * Data constructor.
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     * @param ScopeConfigInterface $scopeConfig
     * @param Context $context
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        ScopeConfigInterface $scopeConfig,
        Context $context,
        ProductRepositoryInterface $productRepository
    ) {
        $this->serializer = $serializer;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_date = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface')->date();
        $this->_productRepository = $productRepository;
        $this->_stockRegistry = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface');
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->_product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->_product = $product;
    }

    protected function _getBackOrderText()
    {
        return $this->_scopeConfig->getValue('delivery_estimate/delivery_estimate/backorder_estimate_notification', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    protected function _checkBackOrder($product)
    {
        $stockItem = $this->_stockRegistry->getStockItem($product->getId(), $product->getStore()->getWebsiteId());
        if ($stockItem->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
            return false;
        } else {
            return $stockItem->getBackorders() > 0 && $stockItem->getQty() <= 0;
        }
    }

    public function isBackorder()
    {
        return $this->_isBackorder;
    }

    /**
     * Return text delivery estimate
     *
     * @return string | null
     */
    public function getTextDeliveryEstimated($product)
    {
        $this->_isBackorder = false;
        $enable = $this->_scopeConfig->getValue('delivery_estimate/delivery_estimate/enable_disable_delivery_estimate', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $cutOffTime = $this->_getCutOffTime();
        $adjustmentTime = 0;
        $correctionTime = 0;
        $alternateTime = 0;
        $leadTime = $this->_scopeConfig->getValue('delivery_estimate/delivery_estimate/lead_time', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $durationTime = $this->_scopeConfig->getValue('delivery_estimate/delivery_estimate/duration_time', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $currentTime = $this->_date->format('H:i');
        $today = $this->_date->format('d-m-Y');
        $tomorrow = date('d-m-Y', strtotime('+1 day'));
        $todayisthursday = $this->_isToday() == 4;
        $todayisfriday = $this->_isToday() == 5;
        $todayissaturday = $this->_isToday() == 6;
        $todayissunday = $this->_isToday() == 0;

        // Check enable / disable
        if (!$enable) {
            return null;
        }

        // Check if lead time is null
        if (is_null($leadTime)) {
            $leadTime = 0;
        }

        // Check if duration time is null
        if (is_null($durationTime)) {
            $durationTime = 1;
        }

        // Product delivery estimate
        if ($product) {
            if ($text = $product->getStfDeliveryEstimate()) {
                return $text;
            }
        }

        // Backorder delivery estimate
        if ($this->_checkBackOrder($product)) {
            $this->_isBackorder = true;

            // If there is back order text
            if ($this->_getBackOrderText()) {
                return $this->_getBackOrderText();
            } else {
                return __('Delayed Shipment');
            }
        }

        // Check if the time now is below cut off time
        if (strtotime($currentTime) <= strtotime($cutOffTime)) {
            $adjustmentTime = 0;
        } else {
            $adjustmentTime = 1;
        }

        if ($this->_isDeliveredInWeekend() == true && $this->_isOutgoingInWeekend() == false) {
            // (DELIVERY IN WEEKEND)
            if ($adjustmentTime == 0 && $todayisfriday) {
                $adjustmentTime = 0;
            } elseif ($todayissaturday || $todayissunday) {
                $adjustmentTime = 1;
            }
        } elseif ($this->_isDeliveredInWeekend() == true && $this->_isOutgoingInWeekend() == true) {
            // (OUTGOING IN WEEKEND - Saturday outgoing only)
            if ($todayisfriday) {
                if ($adjustmentTime == 0) {
                    $correctionTime = -1;
                }
            } elseif ($todayissaturday) {
                if ($adjustmentTime == 0) {
                    $adjustmentTime = 1;
                    $correctionTime = -1;
                } else {
                    $adjustmentTime = 1;
                    $correctionTime = 0;
                }
            } elseif ($todayissunday) {
                $adjustmentTime = 1;
                $correctionTime = 0;
            } else {
            // (OUTGOING IN ALL WEEKEND - Outgoing 7 days a week)
                $correctionTime = 0;
            }
        } else {
            // (NORMAL)
            if ($this->_isWeekend($today)) { // Force delivery delay ordered in weekend
                $adjustmentTime = 1;
            }
            if ($this->_isWeekend($tomorrow)) { // Skip tomorrow delivery (fixes for Friday)
                $alternateTime = 1;
            }
        }

        // Force delay during to weekend and holiday
        if ($this->_isHoliday($today) || $this->_isHoliday($tomorrow)) {
            $adjustmentTime = 1;
        }

        // If possible to ship tomorrow
        if ($adjustmentTime == 0 && $correctionTime == 0 && $alternateTime == 0 && $leadTime == 0 && $durationTime == 1) {
            return __('Order Before %1, Delivered Tomorrow!', $cutOffTime);
        }

        return __('Estimated Delivery on %1', $this->_getWorkingDay($adjustmentTime + $correctionTime + $leadTime + $durationTime, $today));
    }

    protected function _getCutOffTime()
    {
        return date("G:i", strtotime($this->_scopeConfig->getValue('delivery_estimate/delivery_estimate/cut_off_time', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)));
    }

    protected function _isDeliveredInWeekend($param = null)
    {
        $param = $this->_scopeConfig->getValue('delivery_estimate/delivery_estimate/delivered_in_weekend', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if ($param == -1 || is_null($param)) {
            return false;
        } else {
            return $param;
        }
    }

    protected function _isOutgoingInWeekend($param = null)
    {
        $param = $this->_scopeConfig->getValue('delivery_estimate/delivery_estimate/outgoing_in_weekend', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if (is_null($param)) {
            return 0;
        } else {
            return $param;
        }
    }

    protected function _isToday($date = null)
    {
        // 0 => Sunday
        // 1 => Monday
        // 2 => Tuesday
        // 3 => Wednesday
        // 4 => Thursday
        // 5 => Friday
        // 6 => Saturday

        // Make today if there is no input
        if (is_null($date)) {
            $date = $this->_date->format('d-m-Y');
        }

        return date('w', strtotime($date));
    }

    protected function _isWeekend($date = null)
    {
        if (is_null($date)) {
            $convertedDate = $this->_isToday();
        } else {
            $convertedDate = $this->_isToday($date);
        }

        // Check if converted date is within the weekend parameter
        return in_array($convertedDate, $this->_getWeekendDays());
    }

    protected function _getWeekendDays()
    {
        $storeWeekendDays = explode(',', $this->_scopeConfig->getValue('general/locale/weekend', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        $shipmentWeekendDays = explode(',', $this->_isDeliveredInWeekend());

        if ($this->_isDeliveredInWeekend() == true && $this->_isOutgoingInWeekend() == false) {
            // If today is Thursday (force )
            if ($this->_isToday() == 4) {
                $storeWeekendDays = [0];
            }
        } elseif ($this->_isDeliveredInWeekend() == true && $this->_isOutgoingInWeekend() == true) {
            // Make weekend as working day when it's part of outgoing shipping days (CAUTION!!!!! when using isWeekend parameter)
            foreach ($storeWeekendDays as $key => $day) {
                if (in_array($day, $shipmentWeekendDays)) {
                    unset($storeWeekendDays[$key]);
                }
            }
        }

        return $storeWeekendDays;
    }

    public function getHoliday()
    {
        $storeField = $this->_scopeConfig->getValue('general/opening_holiday/holidays', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $storeField;
    }

    protected function _isHoliday($date = null)
    {
        $date = $date ? $date : date('d-m-Y');
        $holidays = $this->serializer->unserialize($this->getHoliday());

        if ($holidays && !empty($holidays)) {
            foreach ((array)$holidays as $holiday) {
                if (gettype($holiday) == "array") {
                    if ($holiday['day'] == date('d', strtotime($date)) && $holiday['month'] == date('m', strtotime($date)))
                        return true;
                }
            }
        }

        return false;
    }

    protected function _getWorkingDay($days, $today, $format = true)
    {
        $addDays = 0;
        $i = 1;
        for ($i; $i <= $days; $i++) {
            $newDate = date('d-m-Y', strtotime($today . ' +' . $i . ' day'));
            if ($this->_isWeekend($newDate) || $this->_isHoliday($newDate)) {
                $addDays++;
            }
        }

        $newDate = date('d-m-Y', strtotime($today . ' +' . $days . ' day'));
        if ($addDays || $this->_isWeekend($newDate) || $this->_isHoliday($newDate)) {
            return $this->_getWorkingDay($addDays, $newDate, $format);
        }

        if (!$format) {
            return $newDate;
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $newDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface')->formatDate(new \DateTime($newDate), \IntlDateFormatter::FULL, false);
        return $newDate;
    }

    public function getCurrentProductId()
    {
        return $this->getProduct()->getId();
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }

    public function isAllowShowingProductListPage()
    {
        $enable = $this->_scopeConfig->getValue('delivery_estimate/delivery_estimate/enable_disable_delivery_estimate', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $enable && $this->_scopeConfig->getValue('delivery_estimate/delivery_estimate/enable_show_on_product_list_page', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
