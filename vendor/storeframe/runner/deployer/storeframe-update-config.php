<?php
	// List Updates Array Here (Don't forget to add the corresponding yaml files in the correct runner directories)
	// vendor/storeframe/runner/config/store/update/
	$updatelist = array(
		"clean-admin", // Cleanup Admin
		"adobe-stock", // Adobe Stock & Image Quality
		"firecheckout-terms", // Firecheckout Terms
		"cache-warmer", // Cache Warmer
		"trash-manager", // Trash Manager
		"admin-setting", // Admin security, sharing and setting
		"soldtogether-count", // Sold together count and layout
		"vat-eu-countries" // Set default EU Countries and VAT for EU & NL
	);
?>
