<?php
// Deployer Plus Script for CC
namespace Deployer;

// Use timestamp for release name
set('release_name', function () {
	return date('d-m-Y_H-i');
});

// Set CC project path
set('cc_project_path', '/home/app/{{hostname}}');
// Set CC default deployer path (To group deployer into one directory)
set('deploy_path', '{{cc_project_path}}/deployer');
// Magento directory
set('magento_dir', '.');
// Magento directory name
set('magento_dir_name', 'public');
// Magento repository (Setting repository is a MUST for Deployer Plus)
set('magento_repository', '');
// Set StoreFrame blank repository
set('stf_blank_repository', 'https://github.com/Storeframe/blank.git');
// Space separated list of languages for static-content:deploy
set('languages', 'en_US nl_NL');
// Force static content deploy and add jobs
set('static_deploy_options', '-f --jobs=8');
// Set CC deploy sudo
set('cc_deploy_sudo', 'sudo');
// Set CC deploy user
set('cc_deploy_user', 'app');
// Set CC deploy group
set('cc_deploy_group', '{{cc_deploy_user}}');
// Flush caches
set('flush_opcache', '');
set('flush_redis', '');
set('flush_varnish', '');
// Set StoreFrame composer options
set('composer_options', 'install --verbose --no-dev --prefer-dist --no-progress --no-interaction --optimize-autoloader --no-suggest');

// Set CC composer path
set('bin/composer', function () {
	return run('which composer');
});

// Set Git repository
set('repository', function () {
	$magerepo = get('magento_repository');
	$blankrepo = get('stf_blank_repository');
	if ($magerepo == '') {
		return $blankrepo;
	} else {
		return $magerepo;
	}
});

// Set Previous Release
set('previous_release', function () {
	$releases = get('releases_list');
	$previousRelease = get('deploy_path') . '/releases/' . $releases[1];
	return $previousRelease;
});

// Set CC Final Flush Cache & Restart
task('stf:restart', function () {
	run('{{flush_opcache}}');
	run('{{flush_redis}}');
	run('{{flush_varnish}}');

	if (test("[ -f {{deploy_path}}/.dep/firstrun.lock ]")) {
	} else {
		print('Restart after first deployer run');
		run('rm -rf {{deploy_path}}/.dep/firstrun.lock');
	}

	run('{{bin/php}} {{release_path}}/{{magento_bin}} indexer:reset && {{bin/php}} {{release_path}}/{{magento_bin}} indexer:reindex');
	run('{{bin/php}} {{release_path}}/{{magento_bin}} cache:enable && {{bin/php}} {{release_path}}/{{magento_bin}} cache:flush');
});
after('cleanup', 'stf:restart');

// Set CC Cron Run (Only on Production)
task('stf:cron', function () {
    run('{{bin/php}} {{release_path}}/{{magento_bin}} cron:remove');
	run('{{bin/php}} {{release_path}}/{{magento_bin}} cron:install');
})->onStage('production');
before('stf:restart', 'stf:cron');

// Set CC Deployer Init Lock (Run Once. To convert to Deployer, Run at Start)
task('stf:init_lock', function () {
	if (test("[ -f {{deploy_path}}/.dep/init.lock ]")) {
		print('init.lock exist, nothing to run');
	} else {
		print('Setting deployer plus for the first time');
		run('mkdir -p {{deploy_path}}/.dep/ {{deploy_path}}/shared/app/etc/ {{deploy_path}}/shared/pub/ {{deploy_path}}/shared/ {{deploy_path}}/storeframe/');
		run('cp {{cc_project_path}}/{{magento_dir_name}}/app/etc/env.php {{deploy_path}}/shared/app/etc/env.php');
		run('cp {{cc_project_path}}/{{magento_dir_name}}/composer.json {{deploy_path}}/storeframe/composer.json');
		run('cp {{cc_project_path}}/{{magento_dir_name}}/composer.lock {{deploy_path}}/storeframe/composer.lock');
		run('touch {{deploy_path}}/.dep/init.lock && touch {{deploy_path}}/.dep/firstrun.lock');
	}
});
before('deploy:prepare', 'stf:init_lock');

// Set CC Deployer Symlink Lock (Run Once. To convert to Deployer, Run at End)
task('stf:symlink_lock', function () {
	if (test("[ -f {{deploy_path}}/.dep/symlink.lock ]")) {
		print('Symlink already created, nothing to run');
	} else {
		print('Setting symlink for the first time');
		run('rm -rf {{deploy_path}}/shared/pub/media && cp -r {{cc_project_path}}/{{magento_dir_name}}/pub/media {{deploy_path}}/shared/pub/media');
		if (test("[ -d '{{cc_project_path}}/{{magento_dir_name}}/sitemaps' ]")) {
			run('cp -r {{cc_project_path}}/{{magento_dir_name}}/sitemaps {{release_path}}');
			run('ln -s {{release_path}}/sitemaps {{release_path}}/pub/sitemaps');
		}
		run('mv {{cc_project_path}}/{{magento_dir_name}} {{cc_project_path}}/{{magento_dir_name}}_origin');
		run('ln -s {{deploy_path}}/current {{cc_project_path}}/{{magento_dir_name}}');
		run('touch {{deploy_path}}/.dep/symlink.lock');
	}
});
before('stf:restart', 'stf:symlink_lock');

// Set StoreFrame Default Composer (Blank Repo, Recurring)
task('stf:master_composer', function () {
	if (get('repository') == get('stf_blank_repository')) {
		print('Deploy Using StoreFrame Master Composer');
		run('cp {{deploy_path}}/storeframe/composer.json {{release_path}}/composer.json');
		run('cp {{deploy_path}}/storeframe/composer.lock {{release_path}}/composer.lock');
		run('cd {{release_path}} && {{bin/composer}} install --verbose --no-dev --prefer-dist --no-progress --no-interaction --optimize-autoloader --no-suggest --ignore-platform-reqs');
	} else {
		print('Continue Using User Defined Composer Repository');
	}
});
before('deploy:vendors', 'stf:master_composer');

// Set StoreFrame Child Theme (Blank Repo, Recurring)
task('stf:child_theme', function () {
	if (get('repository') == get('stf_blank_repository')) {
		print('Deploying StoreFrame child theme');
        run('cd {{release_path}} && mkdir -p app/design/frontend/Storeframe && cp -rn vendor/storeframe/theme-child app/design/frontend/Storeframe');
        run('cd {{release_path}} && mv app/design/frontend/Storeframe/theme-child app/design/frontend/Storeframe/child');
		run('cd {{release_path}} && cp Gruntfile.js.sample Gruntfile.js');
        run('cd {{release_path}} && cp vendor/storeframe/runner/templates/setup/grunt-config.json grunt-config.json');
        run('cd {{release_path}} && cp vendor/storeframe/runner/templates/setup/.gitignore .gitignore');
	} else {
		print('Nothing to process');
	}
});
after('deploy:vendors', 'stf:child_theme');

// Set StoreFrame Production Mode
task('stf:production_setting', function () {
	run('{{bin/php}} {{release_path}}/{{magento_bin}} de:mo:set production -s');
});
after('config:import', 'stf:production_setting');

// Copy Sitemaps Directory and Create Symlink to Pub (Recurring)
task('stf:sitemaps_directory', function () {
	if (get('previous_release')) {
		if (test("[ -d '{{previous_release}}/sitemaps' ]")) {
			run('cp -r {{previous_release}}/sitemaps {{release_path}}');
		} else {
			run('mkdir -p {{release_path}}/sitemaps');
		}
		run('ln -s {{release_path}}/sitemaps {{release_path}}/pub/sitemaps');
	}
});
before('files:generate', 'stf:sitemaps_directory');

// Update Store Config Once (Run Once)
task('stf:update_config', function () {
	// Require storeframe-update-config.php (array of config updates related to yaml files)
	require (dirname(__FILE__) . '/storeframe-update-config.php');

	if (get('stage') == 'dev') {
		// If evironment is dev
		set('mod_deploy_path','');
		set('cd_config_path','');
		run('mkdir -p {{mod_deploy_path}}.dep/config');
	} else {
		// If evironment not dev
		set('mod_deploy_path','{{deploy_path}}/');
		set('cd_config_path','cd {{release_path}} && ');
		run('mkdir -p {{mod_deploy_path}}.dep/config');
	}

	foreach ($updatelist as $value) {
		if (!test("[ -f {{mod_deploy_path}}.dep/config/".$value.".lock ]")) {
			run('{{cd_config_path}}{{bin/php}} {{magento_bin}} config:data:import vendor/storeframe/runner/config/store update/'.$value);
			run('touch {{mod_deploy_path}}.dep/config/'.$value.'.lock');
		}
	};

    print('Config updated, Ready to Rumble!');
});
after('maintenance:unset', 'stf:update_config');

// Collect Missing Translation and Make it Accessible in Admin
task('stf:missing_translation', function () {
    $appliedLanguage = explode(" ", get('languages'));

    foreach ($appliedLanguage as $value) {
        run('{{bin/php}} {{release_path}}/{{magento_bin}} experius_missingtranslations:collect --magento --locale '.$value);
    };
});
after('stf:update_config', 'stf:missing_translation');
