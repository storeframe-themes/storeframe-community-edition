# StoreFrame Runner Library

This composer library includes 3 things.
1. Store Config (Implement init StoreFrame configurations) - To be executed during StoreFrame init
2. Static Blocks (Exported StoreFrame default pages & static blocks) - To be imported during StoreFrame init
3. Template Setup (Magento2 root directory files required to work with StoreFrame. ie: grunt-config.json and .gitignore)
4. StoreFrame Deployer Recipe (Required for deployments)
5. StoreFrame Updater Recipe (Global update new configuration parameter for StoreFrame, run once whenever not initiated on all servers)

*StoreFrame Updater Recipe runs php bin/magento config:data:import vendor/storeframe/runner/config/store update/...
So a yaml updater is required