<?php

namespace Storeframe\SmartLookDebug\Block\Config;

/**
 * @api
 * @since 100.0.2
 */
class DebugKey extends \Magento\Config\Block\System\Config\Form\Field
{
    public function __construct(
        \Magento\Backend\Block\Template\Context $context, array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $html = '';
        $html .= $om->get('Storeframe\SmartLookDebug\Helper\Data')->getDebugLink();
        return $html;
    }
}
