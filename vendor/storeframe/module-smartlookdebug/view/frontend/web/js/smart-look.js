define(
    [
        'jquery',
    ],
    function ($) {
        'use strict';
        $.widget('bss.smart_look', {
            options: {
                isEnableSmartLookDebugInConfig: '',
                isEnableSmartLookDebugAllTraffic: '',
                debugKey: '',
                projectKey: '',
                isMatched: '',
                isLogin: '',
                customerName: '',
                customerEmail: null,
                customerId: null
            },
            _create: function () {
                var self = this;
                if (this.options.isMatched) {
                    self.setCookieX('smartlook_debug', self.options.isMatched, 1);
                }
                window.smartlook||(function(d) {
                    if(!self.isEnableSmartLookDebug()){
                        return;
                    }
                    var o=window.smartlook=function(){ o.api.push(arguments)},
                        h=d.getElementsByTagName('head')[0];
                    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
                    c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
                })(document);
                if (self.isEnableSmartLookDebug()) {
                    window.smartlook('init', this.options.projectKey);
                }
                if (self.options.isLogin) {
                    if(self.isEnableSmartLookDebug()){window.smartlook('tag', 'email', this.options.customerEmail);}
                    if(self.isEnableSmartLookDebug()){window.smartlook('tag', 'name', this.options.customerName);}
                    if(self.isEnableSmartLookDebug()){window.smartlook('tag', 'id', this.options.customerId);}
                }
            },
            setCookieX: function (cname, cvalue, exdays) {
                window.sessionStorage[cname] = cvalue;
            },

            getCookieX: function (cname) {
                var result = window.sessionStorage[cname];
                if(result === undefined){
                    return "";
                }
                return result;
            },

            isEnableSmartLookDebug: function () {
                if (this.options.isEnableSmartLookDebugInConfig == '0') {
                    return false;
                }

                if (this.options.isEnableSmartLookDebugAllTraffic == '1') {
                    return true;
                }
                return this.getCookieX('smartlook_debug') == this.options.debugKey;
            },
        });
        return $.bss.smart_look;
    });
