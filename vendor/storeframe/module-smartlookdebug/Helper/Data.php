<?php

namespace Storeframe\SmartLookDebug\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Session\SessionManagerInterface;


class Data extends AbstractHelper
{

    /**
     * Name of Cookie that holds private content version
     */
    CONST COOKIE_NAME = 'smartlook_debug';

    /**
     * Cookie life time
     */
    CONST COOKIE_LIFE = 86400;

    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    protected $cookieMetadataFactory;

    /**
     * @var $scopeConfigInterface
     */
    private $scopeConfigInterface;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $sessionManager;
    protected $http;


    public function __construct(
        ScopeConfigInterface $scopeConfigInterface,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager,
        \Magento\Framework\App\Request\Http $http,
        Context $context
    )
    {
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
        $this->http = $http;
        parent::__construct($context);
    }

    /**
     * Get data from cookie set in remote address
     *
     * @return value
     */
    public function getCookie($name = null)
    {
        return isset($_COOKIE[$name]) ? $_COOKIE[$name] : null;
    }


    protected function _getDebugKeyFromQueryUrl()
    {
        return $this->http->getParam('debug');
    }

    public function isMatched()
    {
        if (!$this->getEnableFromConfig()) {
            return false;
        }

        $debugKeyFromConfig = $this->getDebugKeyFromConfig();
        $debugKeyFromUrl = $this->_getDebugKeyFromQueryUrl();
        if ($debugKeyFromUrl != $debugKeyFromConfig) {
            return false;
        }
        return $debugKeyFromConfig;
    }

    public function isEnable()
    {
        if (!$this->getEnableFromConfig()) {
            return false;
        }
        return $this->isMatched();
    }

    public function getDebugKeyFromConfig()
    {
        return hash('sha256', $this->getBaseUrl(), false);
    }

    public function getDebugUrl()
    {
        return $this->getBaseUrl() . '?debug=' . $this->getDebugKeyFromConfig();
    }

    public function getDebugLink()
    {
        return '<a href="' . $this->getDebugUrl() . '" target="_blank">' . $this->getDebugKeyFromConfig() . '</a>';
    }

    public function isEnableSmartLookDebugAllTraffic()
    {
        return $this->scopeConfig->getValue('smart_look_debug/smart_look_debug/record_scope', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 2;
    }

    public function getBaseUrl()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('Magento\Store\Model\StoreManager');
        return $storeManager->getStore()->getBaseUrl();
    }

    public function getEnableFromConfig()
    {
        return $this->scopeConfig->getValue('smart_look_debug/smart_look_debug/enable_disable_smart_look_debug', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getJsonConfig()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $dir = $objectManager->get('Magento\Framework\Module\Dir');
        /*Retrieve full path to a directory of certain type within a module*/
        return $dir->getDir('Smartsupp_Smartlook');
    }

    public function getProjectKey()
    {
        return $this->scopeConfig->getValue('smart_look_debug/smart_look_debug/project_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
