<?php
namespace Storeframe\SmartLookDebug\Model\Config\Record;

/**
 * @api
 * @since 100.0.2
 */
class Scope implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 1, 'label' => __('Special traffic')], ['value' => 2, 'label' => __('All traffic')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [1 => __('Special traffic'), 2 => __('All traffic')];
    }
}
