<?php

namespace Storeframe\CheckoutContactForm\Block\Popup;

class Link extends \Magento\Framework\View\Element\Template
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function isEnable()
    {
        return $this->_scopeConfig->getValue('checkoutcontact/checkout_contact_form/popup_enable');
    }

    public function getShowContactLinkTimeout()
    {
        return (1000 * $this->_scopeConfig->getValue('checkoutcontact/checkout_contact_form/popup_time'));
    }
}