<?php

namespace Storeframe\QuantityDropdown\Model\Stock;

class Item extends \Magento\CatalogInventory\Model\Stock\Item
{

    const CUSTOM_QTY = 'custom_qty';

    const ENABLE_CUSTOM_QTY = 'enable_custom_qty';

    /**
     * Retrieve Custom Qty for Quantity Dropdown in Product
     *
     * @return string
     */
    public function getCustomQty()
    {
        return $this->getData(static::CUSTOM_QTY);
    }

    /**
     * Retrieve Enable Custom Qty for Quantity Dropdown in Product
     *
     * @return boolean
     */
    public function getEnableCustomQty()
    {
        return $this->getData(static::ENABLE_CUSTOM_QTY);
    }


}