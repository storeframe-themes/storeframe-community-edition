<?php

namespace Storeframe\QuantityDropdown\Block\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;

class View extends \Magento\Catalog\Block\Product\View {

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Url\EncoderInterface $urlEncoder
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Customer\Model\Session $customerSession
     * @param ProductRepositoryInterface|\Magento\Framework\Pricing\PriceCurrencyInterface $productRepository
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param array $data
     * @codingStandardsIgnoreStart
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Customer\Model\Session $customerSession,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        array $data = [],
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data
        );
    }

    /**
     * Return Custom Quantity by Product
     *
     * @param $product
     * @return array
     */
    public function getCustomQtyByProduct($product = null) {
        if (!$product)
            $product = $this->getProduct();

        $stockItem = $this->stockRegistry->getStockItem($product->getId(), $product->getStore()->getWebsiteId());
        if ($stockItem instanceof \Magento\CatalogInventory\Model\Stock\Item) {
            if ($stockItem->getUseConfigCustomQty()) {
                return explode(",", $this->_scopeConfig->getValue('quantitydropdown/configaction/custom_qty',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
            }
            return explode(",", $stockItem->getCustomQty());
        }
    }

    /**
     * Return Enable Custom Quantity by Product
     *
     * @param $product
     * @return boolean
     */
    public function getEnableCustomQtyByProduct($product = null) {
        if (!$product)
            $product = $this->getProduct();

        $stockItem = $this->stockRegistry->getStockItem($product->getId(), $product->getStore()->getWebsiteId());
        if ($stockItem instanceof \Magento\CatalogInventory\Model\Stock\Item) {
            if ($stockItem->getUseConfigEnableCustomQty()) {
                return $this->_scopeConfig->getValue('quantitydropdown/configaction/enable_custom_qty', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            }
            return $stockItem->getEnableCustomQty();
        }
    }

    /**
     * Return Enable Custom Quantity by Product
     *
     * @return boolean
     */
    public function getEnableContactFormByProduct() {
        return $this->_scopeConfig->getValue('quantitydropdown/configaction/enable_quote', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Get Product Id
     *
     * @param $product
     * @return int
     */
    public function getProductId($product = null) {
        if (!$product) {
            $product = $this->getProduct();
        }

        return $product->getId();
    }
	
	public function getStoreConfig($path)
	{
		$storeField = $this->_scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		return $storeField;
	}
}
