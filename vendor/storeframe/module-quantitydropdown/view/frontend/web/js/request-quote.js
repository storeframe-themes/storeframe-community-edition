define(
    [
        'jquery',
        'Magento_Ui/js/modal/modal',
        'mage/translate',
        'mage/url',
    ],
    function ($, modal, $t, MageUrl) {
        $title = $t('Send Quote');
        var options = {
            type: 'popup',
            innerScroll: false,
            title: $t('Request Quote'),
            modalClass: 'modal-send-quote',
            buttons: [{
                text: $title,
                class: 'action submit primary send-quote',
                click: function () {
                    if (dataForm.validation('isValid')) {
                        $.ajax({
                            url: MageUrl.build('quantitydropdown/product/requestquote'),
                            data
                    :
                        {
                            product: $('#product').val(),
                                name
                        :
                            $('#send-name').val(),
                                email
                        :
                            $('#send-email').val(),
                                phoneNumber
                        :
                            $('#send-phone-number').val(),
                                message
                        :
                            $('#send-message').val()
                        }
                    ,
                        showLoader: true,
                            type
                    :
                        'POST',
                            dataType
                    :
                        'json',
                            success
                    :

                        function (response) {
                            if (response.success) {
                                $('.popup-modal-contact-form').modal('closeModal');
                                // reset form
                                return document.getElementById("request_quote_form").reset();
                            }
                            alert(response.message);
                        }
                    })
                        ;
                        return false;
                    }
                }
            }]
        };
        modal(options, $('.popup-modal-contact-form'));

        $('.request-quote').on('click', function () {
            $('.popup-modal-contact-form').modal('openModal');
        });

        var dataForm = $('#request_quote_form');
        dataForm.mage('validation', {});
    }
);
