define(
    [
        'jquery',
        'mage/mage',
        'Magento_Catalog/product/view/validation',
        'Magento_Catalog/js/catalog-add-to-cart'
    ],
    function ($) {
        return function (config) {
            var customQty = config.customQty.split(','),
                currentQuantity = config.currentQuantity;
            $(document).on('click','.quantity-selected',function () {
                var qty = $(this).data('quantity');
                if (customQty.length > 0) {
                    if (qty !== currentQuantity) {
                        $('.first .title').text(qty);
                        $('#qty').val(qty);
                    }
                } else {
                    $('.first .title').text(qty);
                    $('#qty').val(qty);
                }
                setTimeout(function () {
                    $('.options-wrapper').removeAttr("style");
                    $('.backdrop').removeAttr("style");
                }, 100)
            });
            $('.quantity-selection').click(function() {
                $('.options-wrapper')[0].style.display = "block";
                $('.backdrop')[0].style.display = "block";
            });

            $('.menu-backdrop').click(function() {
                $('.options-wrapper').removeAttr("style");
                $('.backdrop').removeAttr("style");
            });

            $('#product_addtocart_form').mage('validation', {
                radioCheckboxClosest: '.nested',
                submitHandler: function (form) {
                    var widget = $(form).catalogAddToCart({
                        bindSubmit: false
                    });
                    widget.catalogAddToCart('submitForm', $(form));
                    return false;
                }
            });
        }
    }
);
