<?php

namespace Storeframe\QuantityDropdown\Controller\Product;

use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Model\ScopeInterface;

class RequestQuote extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * Catalog image
     *
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_catalogImage = null;

    /**
     * SendFriend data
     *
     * @var \Magento\SendFriend\Helper\Data
     */
    protected $_sendfriendData = null;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
	protected $_scopeConfig;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;


    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Catalog\Helper\Image $catalogImage
     * @param \Magento\SendFriend\Helper\Data $sendfriendData
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Catalog\Helper\Image $catalogImage,
        \Magento\SendFriend\Helper\Data $sendfriendData,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_transportBuilder = $transportBuilder;
        $this->_catalogImage = $catalogImage;
        $this->_sendfriendData = $sendfriendData;
        $this->_storeManager = $storeManager;
		$this->_scopeConfig = $scopeConfig;
        $this->productRepository = $productRepository;
        $this->_coreRegistry = $coreRegistry;
    }


    /**
     * Send Email Post Action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $message = __('Some emails were not sent.');
        $success = false;

        if(!isset($data['product'])) {
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData(['success' => $success, 'message' => $message]);
            return $resultJson;
        }

        $product = $this->_initProduct();
        try {
            $this->send($data, $product);
            $success = true;
            $message = __('The link was sent.');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $message = $e->getMessage();
        } catch (\Exception $e) {
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData(['success' => $success, 'message' => $message]);
        return $resultJson;
    }

    /**
     * @return $this
     * @throws CoreException
     */
    public function send($data, $product)
    {
        $message = nl2br(htmlspecialchars($data['message']));
		
		$senderEmailIdentity = $this->_scopeConfig->getValue('quantitydropdown/configaction/sender_email_identity',ScopeInterface::SCOPE_STORE);
		$email = $this->_scopeConfig->getValue('trans_email/ident_'.$senderEmailIdentity.'/email',ScopeInterface::SCOPE_STORE);
        $template = $this->_scopeConfig->getValue('quantitydropdown/configaction/request_quote_email_template',ScopeInterface::SCOPE_STORE);
		$name  = $this->_scopeConfig->getValue('trans_email/ident_'.$senderEmailIdentity.'/name',ScopeInterface::SCOPE_STORE);
		$storeName = $this->_scopeConfig->getValue('general/store_information/name',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $sender = [
            'name' => $name, // This is need to implement get name website
            'email' => $email, // This is need to implement get email website
        ];



		$imageUrl = $this->_catalogImage->init($product, 'product_page_image_small')
                ->setImageFile($product->getSmallImage()) // image,small_image,thumbnail
                ->resize(380)
                ->getUrl();
				
        $this->_transportBuilder->setTemplateIdentifier(
            $template
        )->setTemplateOptions(
            [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $this->_storeManager->getStore()->getId(),
            ]
        )->setFrom(
            $sender
        )->setTemplateVars(
            [
				'subject' => __('We have received your quote on') . ' ' . $product->getName() . ' - ' . $storeName,
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phoneNumber'],
                'product_name' => $product->getName(),
                'product_url' => $product->getUrlInStore(),
                'message' => $message,
                'sender_name' => $name, // This is need to implement get name website
                'sender_email' => $email, // This is need to implement get email website
                'product_image' => $imageUrl,
                'product_sku' => $product->getSku(),
            ]
        )->addTo(
            $data['email'],
            $data['name']
        )->addCc($email);

        $transport = $this->_transportBuilder->getTransport();
        $transport->sendMessage();
        return $this;
    }

    /**
     * Initialize Product Instance
     *
     * @return \Magento\Catalog\Model\Product
     */
    protected function _initProduct()
    {
        $productId = (int)$this->getRequest()->getParam('product');
        if (!$productId) {
            return false;
        }
        try {
            $product = $this->productRepository->getById($productId);
            if (!$product->isVisibleInCatalog()) {
                return false;
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $noEntityException) {
            return false;
        }

        $this->_coreRegistry->register('product', $product);
        return $product;
    }
}
