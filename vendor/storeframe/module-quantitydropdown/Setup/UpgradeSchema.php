<?php

namespace Storeframe\QuantityDropdown\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if ($context->getVersion() < '1.0.5') {
            $setup->getConnection()->addColumn(
                $setup->getTable('cataloginventory_stock_item'),
                'enable_custom_qty',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Enable Quantity Dropdown ',
                ]
            );
			$setup->getConnection()->addColumn(
				$setup->getTable('cataloginventory_stock_item'),
				'custom_qty',
				[
					'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					'length' => 0,
					'nullable' => true,
					'comment' => 'Quantity Dropdown Count',
				]
			);
            $setup->getConnection()->addColumn(
                $setup->getTable('cataloginventory_stock_item'),
                'use_config_custom_qty',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'nullable' => false,
                    'unsigned' => true,
                    'default' => 1,
                    'comment' => 'Use Config Settings'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('cataloginventory_stock_item'),
                'use_config_enable_custom_qty',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'nullable' => false,
                    'unsigned' => true,
                    'default' => 1,
                    'comment' => 'Use Config Settings'
                ]
            );
        }
        $setup->endSetup();
    }
}