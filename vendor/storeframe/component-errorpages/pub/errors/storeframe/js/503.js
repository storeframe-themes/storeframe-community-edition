timer(
    60, // seconds
    function(timeleft) { // called every step to update the visible countdown
        document.getElementById('timer').innerHTML = "Refreshed in "+timeleft+" seconds";
    },
    function() { // what to do after
        document.getElementById('timer').innerHTML = "Reloading...";
        location.reload();
    }
);

function timer(time,update,complete) {
    var start = (new Date().getTime())/1000;
    var interval = setInterval(function() {
        var now = time-((new Date().getTime())/1000-start);
        if( now <= 0) {
            clearInterval(interval);
            complete();
        }
        else {
            var minutes = ('0'+Math.floor(now / 60)).slice(-2);
            var seconds = ('0'+Math.floor(now - minutes * 60)).slice(-2);
            update(minutes+':'+seconds);
        }
    },100); // the smaller this number, the more accurate the timer will be
}
