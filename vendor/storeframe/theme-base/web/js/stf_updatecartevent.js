define([
	'jquery',
	'mod_favico',
	'mod_rgb2hex',
	'Magento_Customer/js/customer-data'
], function($, Favico, rgb2hex, customerData) {
	$(function($){
		// Cart & Update Cart Listener
		// https://github.com/yireo-training/magento2-example-notify-cart-update/blob/master/view/frontend/web/js/widgetObservingCart.js
		cartData = customerData.get('cart');
		cartData.subscribe(function (updatedCart) {
			
			// Setting Styling
			$bColor = $('.favicon-hidden-color').css('backgroundColor');
			$tColor = $('.favicon-hidden-color').css('color');
			$hexbColor = rgb2hex.convert($bColor);
			$hebtColor = rgb2hex.convert($tColor);
			
			// Preparing Favicon
			var favicon = new Favico({
				animation: 'none',
				position: 'downright',
				bgColor: $hexbColor,
				textColor: $hebtColor
			});
			
			// Engage
			var cartUpdateTrigger = function () {
				// Set Favicon
				$counterQty = updatedCart.summary_count;
				favicon.badge($counterQty);
			};
			
			cartUpdateTrigger();
			
			// Scroll to top on mobile
			var winInnW = window.innerWidth;
			if( winInnW < 767 ){
				$("html, body").animate({scrollTop: "0"}, 300);
			}
		});
		
		$('.action.tocart').click(function(){
			$("html").animate({scrollTop: "0"}, 300, function(){
				$(this).click();
			});
		});
	});
});
