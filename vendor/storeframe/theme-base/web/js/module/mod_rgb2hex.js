define([], function() {
	return {
		convert: function (rgb) {
			rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(,\s*\d+\.*\d+)?\)$/);
			return "#" + this.hex(rgb[1]) + this.hex(rgb[2]) + this.hex(rgb[3]);
		},
		
		hex: function(x) {
			return ("0" + parseInt(x).toString(16)).slice(-2);
		}
	}
});