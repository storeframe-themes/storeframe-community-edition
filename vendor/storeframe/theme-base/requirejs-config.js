var config = {
	map: {
		"*": {
			"mod_favico":	"js/module/mod_favico",
			"mod_rgb2hex":	"js/module/mod_rgb2hex",
			"mod_jscrollpane":	"js/module/mod_jscrollpane",
			"mod_mousewheel":	"js/module/mod_mousewheel",
			"stf_updatecartevent":	"js/stf_updatecartevent"
		}
	},
	shim: {
		'stf_updatecartevent': {
			deps: ['jquery', 'mod_favico', 'mod_rgb2hex']
		}
	},
	deps: [
		'stf_updatecartevent'
	]
};