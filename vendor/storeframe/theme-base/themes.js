module.exports = {
    blank: {
        area: 'frontend',
        name: 'Magento/blank',
        locale: 'en_US',
        files: [
            'css/styles-m',
            'css/styles-l',
            'css/email',
            'css/email-inline'
        ],
        dsl: 'less'
    },
    luma: {
        area: 'frontend',
        name: 'Magento/luma',
        locale: 'en_US',
        files: [
            'css/styles-m',
            'css/styles-l'
        ],
        dsl: 'less'
    },
    backend: {
        area: 'adminhtml',
        name: 'Magento/backend',
        locale: 'en_US',
        files: [
            'css/styles-old',
            'css/styles'
        ],
        dsl: 'less'
    },
	base: {
		area: 'frontend',
		name: 'Storeframe/base',
		locale: 'en_US',
		files: [
			'css/styles-m',
			'css/styles-l'
		],
		dsl: 'less'
	},
	child: {
		area: 'frontend',
		name: 'Storeframe/child',
		locale: 'en_US',
		files: [
			'css/styles-m',
			'css/styles-l'
		],
		dsl: 'less'
	},
	admintheme: {
		area: 'adminhtml',
		name: 'Storeframe/admintheme',
		locale: 'en_US',
		files: [
			'css/styles-old',
			'css/styles'
		],
		dsl: 'less'
	}
};
