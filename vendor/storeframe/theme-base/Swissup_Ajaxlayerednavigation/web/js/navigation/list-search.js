define([
    'jquery',
    'Swissup_Ajaxlayerednavigation/js/lib/list.min',
    'mage/translate',
    'mod_jscrollpane',
    'mod_mousewheel'
], function ($, Listjs, $t) {
    'use strict';
    
    return function (navigation) {
        
        var count = navigation.config.searchFormMinCount,
            options = {
                valueNames: ['swissup-option-label', 'swissup-option-count']
            };
        
        $('#layered-filter-block ol.items.list').each(function (i, ol) {
            var items, html, filterContent;
            
            if (typeof Listjs !== 'function') {
                
                return;
            }
            items = $(ol).find('li.item');
            
            filterContent = $(ol).closest('.filter-options-content');
            filterContent.uniqueId();
            
            if (items.length > count) {
                if (!$(ol).parent('.filter-options-content').find('input.search').length) {
                    
                    html = '<input class="search" placeholder="' + $t('Search') + '" />';
                    // $(html).insertBefore(ol);
                    filterContent.prepend(html);
    
                    $(this).parent().addClass('jsp-filter-maxed-height');
                    $(this).parent().jScrollPane();
                }
            }
            
            new Listjs(filterContent.attr('id'), options);
        });
        
        $('#layered-filter-block .filter-options-content input.search').off();
        $('#layered-filter-block .filter-options-content input.search').on('focus', function () {
            var ol = $(this).parent().find('ol.items');
            
            ol.find('.swissup-aln-hidden').removeClass('swissup-aln-hidden');
            ol.find('.swissup-aln-more').hide();
        });
    
        $('.jsp-filter-maxed-height').each(function() {
            $(this).find('input.search').insertBefore($(this));
        });
        
        $('.filter-options-content').each(function(){
            if ($(this).children().height() > 300) {
                $(this).addClass('jsp-filter-maxed-height');
                $(this).jScrollPane();
            }
        });
    };
});
