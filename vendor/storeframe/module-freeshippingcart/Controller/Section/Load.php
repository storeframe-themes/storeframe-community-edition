<?php

namespace Storeframe\FreeShippingCart\Controller\Section;

use Magento\Customer\CustomerData\Section\Identifier;
use Magento\Customer\CustomerData\SectionPoolInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Load extends \Magento\Customer\Controller\Section\Load {

    /**
     * @var \Magento\Framework\Escaper
     */
    private $escaper;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Identifier $sectionIdentifier
     * @param SectionPoolInterface $sectionPool
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Identifier $sectionIdentifier,
        SectionPoolInterface $sectionPool,
        \Magento\Framework\Escaper $escaper = null,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($context, $resultJsonFactory, $sectionIdentifier, $sectionPool, $escaper);
        $this->escaper = $escaper ?: $this->_objectManager->get(\Magento\Framework\Escaper::class);
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        $resultJson->setHeader('Cache-Control', 'max-age=0, must-revalidate, no-cache, no-store', true);
        $resultJson->setHeader('Pragma', 'no-cache', true);
        try {
            $sectionNames = $this->getRequest()->getParam('sections');
            $sectionNames = $sectionNames ? array_unique(\explode(',', $sectionNames)) : null;

            $updateSectionId = $this->getRequest()->getParam('update_section_id');
            if ('false' === $updateSectionId) {
                $updateSectionId = false;
            }
            $response = $this->sectionPool->getSectionsData($sectionNames, (bool)$updateSectionId);
            if(isset($response['cart'])) {
                $freeShipping = $this->_scopeConfig->getValue('carriers/freeshipping/active',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $minOrderAmount = $this->_scopeConfig->getValue('carriers/freeshipping/free_shipping_subtotal',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $enableFreeShipping = $this->_scopeConfig->getValue('carriers/freeshipping/enable_free_shipping',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                if($freeShipping && $enableFreeShipping && $response['cart']['subtotalAmount'] >= $minOrderAmount) {
                    $response['cart']['mess'] = $this->_scopeConfig->getValue('carriers/freeshipping/free_shipping',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                }
            }
        } catch (\Exception $e) {
            $resultJson->setStatusHeader(
                \Zend\Http\Response::STATUS_CODE_400,
                \Zend\Http\AbstractMessage::VERSION_11,
                'Bad Request'
            );
            $response = ['message' => $this->escaper->escapeHtml($e->getMessage())];
        }
        return $resultJson->setData($response);
    }

}