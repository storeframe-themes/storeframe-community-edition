<?php

namespace Storeframe\Adminconfig\Controller\Adminhtml\Product\Test;

use Magento\Backend\App\Action;

/**
 * Class Upload
 */
class Gen extends \Magento\Backend\App\Action
{
    const SKU = 'storeframe-checkout-test-simple';
    const NAME = 'storeframe checkout test simple';
    const QTY = 0;
    const PRICE = 0.10;
    protected $productFactory;
    protected $productRepository;
    /**
     * @var \Magento\Catalog\Model\Config
     */
    protected $catalogConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(Action\Context $context,
                                \Magento\Catalog\Model\ProductFactory $productFactory,
                                \Magento\Catalog\Model\ProductRepository $productRepository,
                                \Magento\Catalog\Model\ConfigFactory $catalogConfig,
                                \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->catalogConfig = $catalogConfig;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Upload file controller action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if ($this->getProduct()) {
            $this->messageManager->addErrorMessage(__('The test product already existed.'));
            $this->_redirect($this->_redirect->getRefererUrl());
        } else {
            $data = [
                'sku' => self::SKU,
                'name' => self::NAME,
                'price' => self::PRICE,
                'description' => self::NAME,
            ];
            $product = $this->productFactory->create();
            $product->unsetData();
            $product->setData($data);
            $attributeSetId = $product->getDefaultAttributeSetId();
            $product
                ->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE)
                ->setAttributeSetId($attributeSetId)
                ->setWebsiteIds([$this->storeManager->getDefaultStoreView()->getWebsiteId()])
                ->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
                ->setStockData(['is_in_stock' => 1, 'manage_stock' => 0, 'use_config_manage_stock' => 0, 'qty' => 1000])
                ->setStoreId(\Magento\Store\Model\Store::DEFAULT_STORE_ID);
            $product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_IN_SEARCH)->save();
            $this->messageManager->addSuccess(__('The test product has been created.'));
            $this->_redirect($this->_redirect->getRefererUrl());
        }
    }

    public function getProduct()
    {
        try{
            return $this->productRepository->get(self::SKU);
        }catch (\Exception $exception){
            return false;
        }
    }
}