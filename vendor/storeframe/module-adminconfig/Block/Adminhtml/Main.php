<?php
namespace Storeframe\Adminconfig\Block\Adminhtml;

class Main extends \Magento\Framework\View\Element\Template
{
    public function getJsValue()
    {
        return $this->_scopeConfig->getValue('devconfig/superadmin_settings/live_chat_js');
    }
}
