<?php
namespace Storeframe\Adminconfig\Block\Adminhtml;

class Logo extends \Magento\Backend\Block\Page\Header
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\Backend\Helper\Data $backendData
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Backend\Helper\Data $backendData,
        array $data = [],
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_storeManager = $storeManager;
        parent::__construct($context, $authSession, $backendData, $data);
    }
    
     public function getLogoImageSrc()
    {
        $isLogin = $this->getLogin();
        if($isLogin){
            $r = $this->_scopeConfig->getValue('devconfig/superadmin_settings/login_screen_logo', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if($r){
                return $this->getMediaUrl('backend/logo/'. $r);
            }
            return "Storeframe_Adminconfig/images/admin-logo.png";
        }
    
        $r = $this->_scopeConfig->getValue('devconfig/superadmin_settings/logo', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if($r){
          return $this->getMediaUrl('backend/logo/'. $r);
        }
        return "Storeframe_Adminconfig/images/admin-logo-sq.png";
    }
    
    public function getMediaUrl($url)
    {
        $mediaUrl = $this ->_storeManager-> getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl.$url;
    }
    
    public function getViewFileUrl($fileId, array $params = [])
    {
        if(strpos($fileId ,'backend/logo') !== false){
            return $fileId;
        }
        return parent::getViewFileUrl($fileId, $params);
    }
    
    public function getLogin()
    {
        return $this->getData('login');
    }
}
