<?php

namespace Storeframe\Adminconfig\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Button extends Field
{
    protected $_template = 'Storeframe_Adminconfig::system/config/button.phtml';
    protected $productRepository;

    public function __construct(
        Context $context,
        array $data = [],
        \Magento\Catalog\Model\ProductRepository $productRepository
    )
    {
        $this->productRepository = $productRepository;
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    public function getCustomUrl()
    {
        return $this->getUrl('adminconfig/product_test/gen');
    }

    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'generate_test_product',
                'onclick' => 'return window.location.href = "'.$this->getCustomUrl().'"',
                'label' => __('Generate Test Product'),
            ]
        );
        return $button->toHtml();
    }

    public function getProduct()
    {
        try{
            return $this->productRepository->get('storeframe-checkout-test-simple');
        }catch (\Exception $exception){
            return false;
        }
    }
}