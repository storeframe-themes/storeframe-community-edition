<?php
namespace Storeframe\AdminOrderGridFilter\Model\OptionSource\Order;

use Magento\Framework\Option\ArrayInterface;
class CountryType implements ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value' => 'billing', 'label' => __('Billing')],
            ['value' => 'shipping', 'label' => __('Shipping')]
        ];
    }
}
