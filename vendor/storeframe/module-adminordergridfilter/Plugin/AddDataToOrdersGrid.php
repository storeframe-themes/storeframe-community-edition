<?php

namespace Storeframe\AdminOrderGridFilter\Plugin;

/**
 * Class AddDataToOrdersGrid
 */
class AddDataToOrdersGrid
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * AddDataToOrdersGrid constructor.
     *
     * @param \Psr\Log\LoggerInterface $customLogger
     * @param array $data
     */
    public function __construct(
        \Psr\Log\LoggerInterface $customLogger,
        array $data = []
    )
    {
        $this->logger = $customLogger;
    }

    /**
     * @param \Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory $subject
     * @param \Magento\Sales\Model\ResourceModel\Order\Grid\Collection $collection
     * @param $requestName
     * @return mixed
     */
    public function afterGetReport($subject, $collection, $requestName)
    {
        if ($requestName !== 'sales_order_grid_data_source' && $requestName !== 'sales_order_invoice_grid_data_source') {
            return $collection;
        }

        $orderTableName = $collection->getResource()->getTable('sales_order');
        if ($collection->getMainTable() === $collection->getResource()->getTable('sales_order_grid')) {
            try {
                $orderAddressTableName = $collection->getResource()->getTable('sales_order_address');
                $collection->getSelect()->joinLeft(
                    ['soa' => $orderAddressTableName],
                    'soa.parent_id = main_table.entity_id',
                    [
                        'soa.country_id', 'soa.address_type',
                        'CONCAT(main_table.billing_address, "\r\n", soa.country_id) as billing_address',
                        'CONCAT(main_table.shipping_address, "\r\n", soa.country_id) as shipping_address',
                        'soa.company',
                    ]
                );
                $collection->getSelect()->joinLeft(
                    ['so' => $orderTableName],
                    'so.entity_id = main_table.entity_id',
                    ['so.coupon_code']
                );
            } catch (\Zend_Db_Select_Exception $selectException) {
                // Do nothing in that case
                $this->logger->log(100, $selectException);
            }
        }

        if ($collection->getMainTable() === $collection->getResource()->getTable('sales_invoice_grid')) {
            try {
                $orderGTableName = $collection->getResource()->getTable('sales_order_grid');
                $orderAddressTableName = $collection->getResource()->getTable('sales_order_address');
                $collection->getSelect()->join(
                    ['sog' => $orderGTableName],
                    'sog.entity_id = main_table.order_id',
                    null
                )->group('sog.entity_id');
                $collection->getSelect()->joinLeft(
                    ['soa' => $orderAddressTableName],
                    'soa.parent_id = sog.entity_id',
                    [
                        'soa.country_id',
                        'soa.address_type',
                        'CONCAT(main_table.billing_address, "\r\n", soa.country_id) as billing_address',
                        'CONCAT(main_table.shipping_address, "\r\n", soa.country_id) as shipping_address',
                        'soa.company',
                    ]
                );
                $collection->getSelect()->joinLeft(
                    ['so' => $orderTableName],
                    'so.entity_id = soa.entity_id',
                    ['so.coupon_code']
                );

            } catch (\Zend_Db_Select_Exception $selectException) {
                // Do nothing in that case
                $this->logger->log(100, $selectException);
            }
        }

        return $collection;
    }
}