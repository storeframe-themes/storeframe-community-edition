<?php
/**
 * Zestard Technologies Private Limited.
 *
 * @category  Zestard_Technologies
 * @package   Zestardtech_Trashmanager
 * @author    Zestard Technologies <info@zestard.com>
 * @copyright 2020 Zestard Technologies Private Limited (https://www.zestard.com)
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace Zestardtech\Trashmanager\Cron;

/**
 * Class ProductTrash
 */
class ProductTrash {

	protected $_logger;

	/**
	 * @var \Magento\Framework\Stdlib\DateTime\DateTime
	 */
	protected $date;

	/**
	 * @var \Zestardtech\Trashmanager\Model\ResourceModel\ProductTrash\CollectionFactory
	 */
	protected $trashproductCollection;

	/**
	 * @var \Zestardtech\Trashmanager\Helper\Data 	
	 */
	private $helper;
	
	/**
	 * Construct
	 * @param \Psr\Log\LoggerInterface                                                    $logger
	 * @param \Magento\Framework\Stdlib\DateTime\DateTime                                 $date
	 * @param \Zestardtech\Trashmanager\Model\ResourceModel\ProductTrash\CollectionFactory $trashproductCollection
	 * @param \Zestardtech\Trashmanager\Helper\Data 										  $helper
	 */
	public function __construct(
		\Psr\Log\LoggerInterface $logger,
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
		\Zestardtech\Trashmanager\Model\ResourceModel\ProductTrash\CollectionFactory $trashproductCollection,
		\Zestardtech\Trashmanager\Helper\Data $helper
	) {
		$this->_logger = $logger;
		$this->date = $date;
		$this->helper = $helper;
		$this->trashproductCollection = $trashproductCollection;
	}

	/**
	 * Method executed when cron runs in server
	 */
	public function execute() {
		$days = $this->helper->getProductTrashAutoCleanDays();
		$date = $this->date->gmtDate('Y-m-d', "-{$days} days");
		$collection = $this->trashproductCollection->create()
			->addFieldToFilter('trashed_at', ['to' => $date.' 00:00:00']);
		$collection->walk('delete');
		$this->_logger->debug('Running Cron from product trash cleaner');
		return $this;
	}
}
