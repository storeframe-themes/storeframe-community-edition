<?php
/**
 * Zestard Technologies Private Limited.
 *
 * @category  Zestard_Technologies
 * @package   Zestardtech_Trashmanager
 * @author    Zestard Technologies <info@zestard.com>
 * @copyright 2020 Zestard Technologies Private Limited (https://www.zestard.com)
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace Zestardtech\Trashmanager\Cron;

/**
 * Class CategoryTrash
 */
class CategoryTrash {

	protected $_logger;

	/**
	 * @var \Magento\Framework\Stdlib\DateTime\DateTime
	 */
	protected $date;

	/**
	 * @var \Zestardtech\Trashmanager\Model\ResourceModel\CategoryTrash\CollectionFactory
	 */
	protected $trashcategoryCollection;

	/**
	 * @var \Zestardtech\Trashmanager\Model\ConfigModel
	 */
	private $configModel;

	/**
	 * Construct
	 * @param \Psr\Log\LoggerInterface                                                     $logger
	 * @param \Magento\Framework\Stdlib\DateTime\DateTime                                  $date
	 * @param \Zestardtech\Trashmanager\Model\ResourceModel\CategoryTrash\CollectionFactory $trashcategoryCollection
	 * @param \Zestardtech\Trashmanager\Helper\Data 										   $helper
	 */
	public function __construct(
		\Psr\Log\LoggerInterface $logger,
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
		\Zestardtech\Trashmanager\Model\ResourceModel\CategoryTrash\CollectionFactory $trashcategoryCollection,
		\Zestardtech\Trashmanager\Helper\Data $helper
	) {
		$this->_logger = $logger;
		$this->date = $date;
		$this->trashcategoryCollection = $trashcategoryCollection;
		$this->helper = $helper;
	}

	/**
	 * Method executed when cron runs in server
	 */
	public function execute() {
		$days = $this->helper->getCategoryTrashAutoCleanDays();
		$date = $this->date->gmtDate('Y-m-d', "-{$days} days");
		$collection = $this->trashcategoryCollection->create()
			->addFieldToFilter('trashed_at', ['to' => $date.' 00:00:00']);
		$collection->walk('delete');
		$this->_logger->debug('Running Cron from Category trash cleaner');
		return $this;
	}
}
