<?php
/**
 * Zestard Technologies Private Limited.
 *
 * @category  Zestard_Technologies
 * @package   Zestardtech_Trashmanager
 * @author    Zestard Technologies <info@zestard.com>
 * @copyright 2020 Zestard Technologies Private Limited (https://www.zestard.com)
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace Zestardtech\Trashmanager\Observer;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\Model\Auth\Session;
use Zestardtech\Trashmanager\Model\ProductTrash;

class ProductDeletePostDispatchObserver implements \Magento\Framework\Event\ObserverInterface
{
	/**
	 * @var \Zestardtech\Trashmanager\Model\ProductTrash
	 */
	protected $productTrashModel;

	/**
	 * @param \Zestardtech\Trashmanager\Model\ProductTrash 	  $productTrashModel
	 */
	public function __construct(
		\Zestardtech\Trashmanager\Model\ProductTrash $categoryTrashModel
	) {
		$this->productTrashModel = $productTrashModel;
	}

	/**
	 * @param \Magento\Framework\Event\Observer $observer
	 * @return void
	 */
	public function execute(\Magento\Framework\Event\Observer $observer)
    {   
    	$request = $observer->getRequest();
    	$id  = $request->getParam('id',false);
    	if($id && $this->productTrashModel->isProductExistInCatalog($id)){
    		$this->productTrashModel->load($id,'entity_id');
    		if($this->productTrashModel->getId()){
    			$this->productTrashModel->delete();
    		}
    	}
    	return $this;
    }
}	