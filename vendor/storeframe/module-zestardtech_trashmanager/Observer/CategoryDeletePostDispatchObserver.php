<?php
/**
 * Zestard Technologies Private Limited.
 *
 * @category  Zestard_Technologies
 * @package   Zestardtech_Trashmanager
 * @author    Zestard Technologies <info@zestard.com>
 * @copyright 2020 Zestard Technologies Private Limited (https://www.zestard.com)
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace Zestardtech\Trashmanager\Observer;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\Model\Auth\Session;
use Zestardtech\Trashmanager\Model\CategoryTrash;

class CategoryDeletePostDispatchObserver implements \Magento\Framework\Event\ObserverInterface
{
	/**
	 * @var \Zestardtech\Trashmanager\Model\CategoryTrash
	 */
	protected $categoryTrashModel;

	/**
	 * @param \Zestardtech\Trashmanager\Model\CategoryTrash 	  $categoryTrashModel
	 */
	public function __construct(
		\Zestardtech\Trashmanager\Model\CategoryTrash $categoryTrashModel
	) {

		$this->categoryTrashModel = $categoryTrashModel;
	}

	/**
	 * @param \Magento\Framework\Event\Observer $observer
	 * @return void
	 */
	public function execute(\Magento\Framework\Event\Observer $observer)
    {   
    	$request = $observer->getRequest();
    	$id  = $request->getParam('id',false);
    	if($id && $this->categoryTrashModel->isCategoryExistInCatalog($id)){
    		$this->categoryTrashModel->load($id,'entity_id');
    		if($this->categoryTrashModel->getId()){
    			$this->categoryTrashModel->delete();
    		}
    	}
    	return $this;
    }
}	