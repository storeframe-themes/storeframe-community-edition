<?php

/**
 * Zestard Technologies Private Limited.
 *
 * @category  Zestard_Technologies
 * @package   Zestardtech_Trashmanager
 * @author    Zestard Technologies <info@zestard.com>
 * @copyright 2020 Zestard Technologies Private Limited (https://www.zestard.com)
 * @license   https://opensource.org/licenses/MIT MIT License
 */
namespace Zestardtech\Trashmanager\Ui\Component\Listing\Column\Category;

use Zestardtech\Trashmanager\Helper\Data as Helper;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class IncludeInMenu
 */
class IncludeInMenu extends Column {
	
	/**
	 * Construct
	 * @param ContextInterface   $context
	 * @param UiComponentFactory $uiComponentFactory
	 * @param array              $components
	 * @param array              $data
	 */
	public function __construct(
		ContextInterface $context,
		UiComponentFactory $uiComponentFactory,
		array $components = [],
		array $data = []
	) {
		parent::__construct($context, $uiComponentFactory, $components, $data);
	}

	/**
	 * Prepare Data Source
	 *
	 * @param array $dataSource
	 * @return array
	 */
	public function prepareDataSource(array $dataSource) {
		if (isset($dataSource['data']['items'])) {
			$visibilityData = $this->getData('name');
			/*$fieldName = $this->getData('')*/
			foreach ($dataSource['data']['items'] as &$item) {
				$json_data = json_decode($item['category_data'], true);
				if(isset($json_data['include_in_menu']))
				{
					$item[$visibilityData] = $json_data['include_in_menu'];	
				}
			}
		}
		return $dataSource;
	}

}