<?php

/**
 * Zestard Technologies Private Limited.
 *
 * @category  Zestard_Technologies
 * @package   Zestardtech_Trashmanager
 * @author    Zestard Technologies <info@zestard.com>
 * @copyright 2020 Zestard Technologies Private Limited (https://www.zestard.com)
 * @license   https://opensource.org/licenses/MIT MIT License
 */
namespace Zestardtech\Trashmanager\Ui\Component\Listing\Column\Category;

use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Path
 */
class Path extends Column {
	/**
	 * Prepare Data Source.
	 *
	 * @param array $dataSource
	 *
	 * @return array
	 */
	public function prepareDataSource(array $dataSource) {
		if (isset($dataSource['data']['items'])) {
			$fieldName = $this->getData('name');
			foreach ($dataSource['data']['items'] as &$item) {
				$data = json_decode($item['category_data'], true);
				if (isset($data[$fieldName])) {
					$item[$fieldName] = $data[$fieldName];
				}
			}
		}
		return $dataSource;
	}
}