<?php
/**
 * Zestard Technologies Private Limited.
 *
 * @category  Zestard_Technologies
 * @package   Zestardtech_Trashmanager
 * @author    Zestard Technologies <info@zestard.com>
 * @copyright 2020 Zestard Technologies Private Limited (https://www.zestard.com)
 * @license   https://opensource.org/licenses/MIT MIT License
 */
namespace Zestardtech\Trashmanager\Model\ProductTrash;

use Zestardtech\Trashmanager\Model\ResourceModel\ProductTrash\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {

	protected $collection;

	protected $dataPersistor;

	protected $loadedData;

	private $storeManager;

	/**
	 * Constructor
	 *
	 * @param string $name
	 * @param string $primaryFieldName
	 * @param string $requestFieldName
	 * @param CollectionFactory $collectionFactory
	 * @param DataPersistorInterface $dataPersistor
	 * @param array $meta
	 * @param array $data
	 */
	public function __construct(
		$name,
		$primaryFieldName,
		$requestFieldName,
		CollectionFactory $collectionFactory,
		DataPersistorInterface $dataPersistor,
		StoreManagerInterface $storeManager,
		array $meta = [],
		array $data = []
	) {
		$this->collection = $collectionFactory->create();
		$this->dataPersistor = $dataPersistor;
		$this->storeManager = $storeManager;
		parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
	}

	/**
	 * Get data
	 *
	 * @return array
	 */
	public function getData() {
		if (isset($this->loadedData)) {
			return $this->loadedData;
		}
		$items = $this->collection->getItems();
		/** @var \Magento\Cms\Model\Block $block */
		foreach ($items as $trash) {

			$this->loadedData[$trash->getId()] = $trash->getData();

		}

		$data = $this->dataPersistor->get('trashmanager');
		if (!empty($data)) {
			// $jsonData1=json_decode($items);
			$trash = $this->collection->getNewEmptyItem();
			$trash->setData($data);

			$model = $objectManager->create('Zestardtech\Trashmanager\Model\ProductTrash');

			$this->loadedData[$trash->getId()] = $trash->getData();
			$this->dataPersistor->clear('trashmanager');
		}

		return $this->loadedData;
	}

}