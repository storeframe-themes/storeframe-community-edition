<?php
/**
 * Zestard Technologies Private Limited.
 *
 * @category  Zestard_Technologies
 * @package   Zestardtech_Trashmanager
 * @author    Zestard Technologies <info@zestard.com>
 * @copyright 2020 Zestard Technologies Private Limited (https://www.zestard.com)
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace Zestardtech\Trashmanager\Model\Visibility\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Visibility implements OptionSourceInterface {
	/**
	 * Get options
	 *
	 * @return array
	 */
	public function toOptionArray() {
		$option = [
			[
				'value' => 1,
				'label' => __('Not Visible Individually'),
			],
			[
				'value' => 2,
				'label' => __('Catalog'),
			],
			[
				'value' => 3,
				'label' => __('Search'),
			],
			[
				'value' => 4,
				'label' => __('Catalog, Search'),
			],
		];
		return $option;

	}
}