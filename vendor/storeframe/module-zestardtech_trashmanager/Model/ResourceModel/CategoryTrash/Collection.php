<?php
/**
 * Zestard Technologies Private Limited.
 *
 * @category  Zestard_Technologies
 * @package   Zestardtech_Trashmanager
 * @author    Zestard Technologies <info@zestard.com>
 * @copyright 2020 Zestard Technologies Private Limited (https://www.zestard.com)
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace Zestardtech\Trashmanager\Model\ResourceModel\CategoryTrash;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
	protected $_idFieldName = 'trash_id';
	/**
	 * Initialize resource
	 *
	 * @return void
	 */
	protected function _construct() {
		$this->_init(\Zestardtech\Trashmanager\Model\CategoryTrash::class, \Zestardtech\Trashmanager\Model\ResourceModel\CategoryTrash::class);
	}
}
