<?php
/**
 * Zestard Technologies Private Limited.
 *
 * @category  Zestard_Technologies
 * @package   Zestardtech_Trashmanager
 * @author    Zestard Technologies <info@zestard.com>
 * @copyright 2020 Zestard Technologies Private Limited (https://www.zestard.com)
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace Zestardtech\Trashmanager\Model\ResourceModel;

class ProductTrash extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
	/**
	 * Constructor
	 *
	 * @param \Magento\Framework\Model\ResourceModel\Db\Context $context,
	 * @param string $connectionName
	 */
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context,
		$connectionName = null
	) {
		parent::__construct($context, $connectionName);
	}

	/**
	 * Define Main Table
	 *
	 * @return void
	 */
	protected function _construct() {
		$this->_init('zestardtech_product_trash', 'trash_id');
	}
}