<?php
/**
 * Zestard Technologies Private Limited.
 *
 * @category  Zestard_Technologies
 * @package   Zestardtech_Trashmanager
 * @author    Zestard Technologies <info@zestard.com>
 * @copyright 2020 Zestard Technologies Private Limited (https://www.zestard.com)
 * @license   https://opensource.org/licenses/MIT MIT License
 */

namespace Zestardtech\Trashmanager\Controller\Adminhtml\Product;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 */
class Index extends \Magento\Backend\App\Action {

	/**
	 * Authorization level of a basic admin session
	 *
	 * @see _isAllowed()
	 */
	const ADMIN_RESOURCE = 'Zestardtech_Trashmanager::product';

	/**
	 * @var PageFactory
	 */
	protected $resultPageFactory;
	protected $objectManager;

	/**
	 * @param Context $context
	 * @param PageFactory $resultPageFactory
	 */
	public function __construct(Context $context, PageFactory $resultPageFactory) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	/**
	 * Index action
	 *
	 * @return \Magento\Backend\Model\View\Result\Page
	 */
	public function execute() {

		$collectionFactory = $this->_objectManager->get(\Magento\Indexer\Model\Indexer\CollectionFactory::class);
		$indexers = $collectionFactory->create()->getItems();

		$indexers = array_combine(
			array_map(
				function ($item) {
					/** @var IndexerInterface $item */
					return $item->getId();
				},
				$indexers
			),
			$indexers
		);

		/** @var \Magento\Backend\Model\View\Result\Page $resultPage */
		$resultPage = $this->resultPageFactory->create();
		$resultPage->setActiveMenu('Zestardtech_Trashmanager::zestardtech_trashmanager');
		$resultPage->addBreadcrumb(__('Trashmanager'), __('Trashmanager'));
		$resultPage->getConfig()->getTitle()->prepend(__('Trashed Products'));

		$dataPersistor = $this->_objectManager->get(\Magento\Framework\App\Request\DataPersistorInterface::class);
		$dataPersistor->clear('zestardtech_trashmanager');

		return $resultPage;
	}

	/**
	 * getIndexers
	 * @return String
	 */
	protected function getIndexers() {
		$collectionFactory = $this->_objectManager->get(\Magento\Indexer\Model\Indexer\CollectionFactory::class);
		$indexers = $collectionFactory->create()->getItems();

		return array_combine(
			array_map(
				function ($item) {
					/** @var IndexerInterface $item */
					return $item->getId();
				},
				$indexers
			),
			$indexers
		);
	}
}