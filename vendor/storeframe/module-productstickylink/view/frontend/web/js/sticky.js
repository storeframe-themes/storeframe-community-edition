define(
    [
        'jquery'
    ],
    function ($) {
        jQuery(function () {
            var currentP = 0;
            var stickyOffset = 0;

            stickyOffset = $('#product-addtocart-button').offset().top;
            stickyOffset += $('#product-addtocart-button').outerHeight();
            stickyOffset += 100;

            $(window).scroll(function () {
                clearTimeout($.data(this, 'scrollTimer'));
                $.data(this, 'scrollTimer', setTimeout(function () {
                    var scrollP = $(window).scrollTop();
                    var winInnW = window.innerWidth;
                    if (winInnW > 767) {
                        if (scrollP != currentP) {
                            if (scrollP > stickyOffset) {
                                $('.product-info-sticky').addClass('visible');
                            } else {
                                $('.product-info-sticky').removeClass('visible');
                            }
                            currentP = $(window).scrollTop();
                        }
                    }
                }, 300));
            });

            $('#sticky-product-addtocart-button').click(function () {
                $("html, body").animate({
                    scrollTop: "0"
                }, 300);
                $("#product-addtocart-button").click();
            });
        });
    });