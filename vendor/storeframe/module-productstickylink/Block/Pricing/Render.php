<?php
namespace Storeframe\ProductStickyLink\Block\Pricing;

class Render extends \Magento\Catalog\Pricing\Render
{
    /**
     * Produce and return block's html output
     *
     * @return string
     */
    protected function _toHtml()
    {
        return parent::_toHtml();
    }
}