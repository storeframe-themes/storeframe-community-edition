<?php
namespace Storeframe\ProductStickyLink\Block;

use Magento\Catalog\Model\Product;

class Sticky extends \Magento\Framework\View\Element\Template
{
    const XML_PATH_STICKY_INFO  = "productstickylink/product_info/";

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * Get Product Sticky Link in product view Configuration
     *
     * @param $attr
     * @return \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public function getProductStickyLinkConfig($attr) {
        $config =  $this->_scopeConfig->getValue(self::XML_PATH_STICKY_INFO . $attr, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $config;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('product');
    }

    /**
     * Retrieve image width
     *
     * @return bool|int
     */
    public function getImageWidth()
    {
        if($this->getProductStickyLinkConfig('image_width')) {
            return $this->getProductStickyLinkConfig('image_width');
        } else {
            return 200;
        }
    }
}