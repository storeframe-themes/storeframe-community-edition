<?php
namespace Storeframe\Themecustomizer\Block;

class Template extends \Magento\Framework\View\Element\Template
{
    public function getConfig($config_path, $storeCode = null)
    {
        return $this->_scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeCode
        );
    }
}
