<?php
namespace Storeframe\Themecustomizer\Block\Adminhtml\System\Config\Form\Field;

class Font extends \Magento\Config\Block\System\Config\Form\Field
{
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $htmlId = $element->getHtmlId();

        $html = parent::_getElementHtml($element);

        $html .= '<br/><div id="'.$htmlId.'_view" style="font-size:20px; margin-top:5px;">The quick brown fox jumps over the lazy dog</div>';
        $html .="
            <script type='text/javascript'>
                require([
                    'jquery'
                ], function(jQuery){
                    (function($) {
                        $('#".$element->getHtmlId()."').change(function  () {
                            $('#".$element->getHtmlId()."_view').css({fontFamily:  $('#".$element->getHtmlId()."').val().replace('+', ' ')});
                            $('<link />', {href: '//fonts.googleapis.com/css?family=' + $('#".$element->getHtmlId()."').val(), rel: 'stylesheet', type:  'text/css'}).appendTo('head');
                        }).keyup(function () {
                            $('#".$element->getHtmlId()."_view').css({ fontFamily: $('#".$element->getHtmlId()."').val().replace('+', ' ')});
                            $('<link />', {href: '//fonts.googleapis.com/css?family=' + $('#".$element->getHtmlId()."').val(), rel: 'stylesheet', type: 'text/css'}).appendTo('head');
                        }).keydown(function () {
                            $('#".$element->getHtmlId()."_ view').css({fontFamily: $('#".$element->getHtmlId()."').val().replace('+', ' ')});
                            $('<link />', {href: '//fonts.googleapis.com/css?family=' + $('#".$element->getHtmlId()."').val(), rel: 'stylesheet', type: 'text/css'}).appendTo('head');
                        });
                        $('#".$element->getHtmlId()."').trigger('change');
                    })(jQuery);
                });
		    </script>
        ";

        return $html;
    }
}
