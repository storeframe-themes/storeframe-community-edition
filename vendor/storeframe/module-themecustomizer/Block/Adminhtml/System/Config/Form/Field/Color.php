<?php
namespace Storeframe\Themecustomizer\Block\Adminhtml\System\Config\Form\Field;

/**
 * Class Color
 * @package Cattheme\Base\Block\Adminhtml\System\Config\Form\Field
 */
class Color extends \Magento\Config\Block\System\Config\Form\Field
{
	/**
	 * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
	 * @return string
	 */
	protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
	{
		$html = $element->getElementHtml();
		$html .= '<script type="text/javascript">var picker = document.getElementById("'. $element->getHtmlId() .'"); picker.className = picker.className + " jscolor {required:false,hash:true}"; </script>';
		return $html;
	}
}
