<?php
namespace Storeframe\Themecustomizer\Block\Html;

class Head extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Storeframe\Themecustomizer\Helper\Data $ctHelper,
        array $data = [])
    {
        parent::__construct($context, $data);
        $_pageConfig = $context->getPageConfig();

        $layout = $ctHelper->getStoreConfig('basecustom/general/site_layout');
        $_pageConfig->addBodyClass($layout);
    }
}