<?php
namespace Storeframe\Themecustomizer\Model\System\Config\Source;

class MobileColumn implements \Magento\Framework\Option\ArrayInterface
{
	public function toOptionArray()
	{
		return [
			['value' => 'null', 'label' => __('--- Select ---')],
			['value' => '1', 'label' => __('1 Column')],
			['value' => '2', 'label' => __('2 Columns')]
		];
	}
}