<?php
namespace Storeframe\Themecustomizer\Model\System\Config\Source;

class Layout implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'full-width-layout', 'label' => __('Default')],
			['value' => 'full-width-expanded', 'label' => __('Expanded Width')],
            ['value' => 'full-width-browser', 'label' => __('Full Browser Width')],
            ['value' => 'boxed-layout', 'label' => __('Boxed Layout')]
        ];
    }
}
