<?php
namespace Storeframe\Themecustomizer\Model\System\Config\Source;

class DesktopxlColumn implements \Magento\Framework\Option\ArrayInterface
{
	public function toOptionArray()
	{
		return [
			['value' => 'null', 'label' => __('--- Select ---')],
			['value' => '1', 'label' => __('1 Column')],
			['value' => '2', 'label' => __('2 Columns')],
			['value' => '3', 'label' => __('3 Columns')],
			['value' => '4', 'label' => __('4 Columns')],
			['value' => '5', 'label' => __('5 Columns')]
		];
	}
}