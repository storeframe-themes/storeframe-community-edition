<?php
namespace Storeframe\Themecustomizer\Model\System\Config\Source;

class Fontstyle implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => '', 'label' => __('--- Select ---')],
            ['value' => 'normal', 'label' => __('Normal')],
            ['value' => 'italic', 'label' => __('Italic')],
            ['value' => 'oblique', 'label' => __('Oblique')],
            ['value' => 'inherit', 'label' => __('Inherit')]
        ];
    }
}