<?php
namespace Storeframe\Themecustomizer\Model\Cssconfig;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Directory\Helper\Data;

class Cssgen
{
    protected $_storeManager;
    protected $_coreRegistry;
    protected $_layoutManager;
    protected $_messageManger;
    protected $_fileSystem;
    protected $_themeModel;
    protected $_scopeConfig;
    protected $_cssConfig;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\LayoutInterface $layoutManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Theme\Model\Theme $themeModel,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Storeframe\Themecustomizer\Helper\Cssconfig $cssConfig
    )
    {
        $this->_storeManager = $storeManager;
        $this->_coreRegistry = $coreRegistry;
        $this->_layoutManager = $layoutManager;
        $this->_messageManager = $messageManager;
        $this->_fileSystem = $filesystem;
        $this->_themeModel = $themeModel;
        $this->_scopeConfig = $scopeConfig;
        $this->_cssConfig = $cssConfig;
    }

    public function generateCss($websiteId,$storeId){
        if(!$websiteId && !$storeId){
            $websites = $this->_storeManager->getWebsites();
            foreach ($websites as $website){
                $this->generateWebsiteCss($website);
            }
            return true;
        }

        if($storeId){
           return $this->generateStoreCss($storeId);
        }
        return $this->generateWebsiteCss($this->_storeManager->getWebsite($websiteId));
    }

    protected function generateWebsiteCss($website){
        if(empty($website)){
            return false;
        }

        foreach ($website->getStoreIds() as $storeId){
            $this->generateStoreCss($storeId);
        }
    }

    protected function generateStoreCss($storeId){
        $store = $this->_storeManager->getStore($storeId);
        if(empty($store)){
            return false;
        }

        if(!$store->isActive()){
            return false;
        }

        $this->_coreRegistry->register('ct_store', $store);
        $cssBlockHtml = $this->_layoutManager->createBlock('Storeframe\Themecustomizer\Block\Template')->setTemplate("Storeframe_Themecustomizer::basecustom.phtml")->toHtml();
        try{

            if(empty($cssBlockHtml)){
                throw new \Exception(__("Template file is empty or doesn't exist."));
            }

            $dir = $this->_fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
            $fileName = $this->_cssConfig->getCssFilePath($store->getCode());
            $dir->writeFile($fileName, $cssBlockHtml);
            $this->_messageManager->addSuccess(__('The %1 file updated successfully.', $dir->getAbsolutePath($fileName)));

        }catch(\Exception $e){
            $this->_messageManager->addError(__('Failed generating CSS file'));
        }
        $this->_coreRegistry->unregister('ct_store');
    }
}
