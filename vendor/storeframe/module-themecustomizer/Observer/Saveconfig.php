<?php
namespace Storeframe\Themecustomizer\Observer;;

class Saveconfig implements \Magento\Framework\Event\ObserverInterface
{
    
    protected $_messageManager;
    protected $_cssGenerator;

    public function __construct(
        \Storeframe\Themecustomizer\Model\Cssconfig\Cssgen $cssGenerator,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->_cssGenerator = $cssGenerator;
        $this->_messageManager = $messageManager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_cssGenerator->generateCss($observer->getData('website'), $observer->getData('store'));
    }
}