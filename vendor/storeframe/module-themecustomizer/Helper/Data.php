<?php
namespace Storeframe\Themecustomizer\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    protected $_storeManager;
    protected $_scopeConfig;
    protected $_coreRegistry;
    
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
       
    )
    {
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_storeManager = $storeManager;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }


    protected function _getStore($storeId = null)
    {
        if($this->_coreRegistry->registry('ct_store')){
            return $this->_coreRegistry->registry('ct_store');
        }

        return $this->_storeManager->getStore($storeId);
    }

    public function getConfig($config_path, $storeId = null)
    {
        $store = $this->_getStore($storeId);
        $r =  $this->_scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store->getId()
        );
        return $r;
    }

    public function getStoreConfig($config_path, $storeId = NULL)
    {
        return $this->_scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->_getStore($storeId)->getId()
        );
    }

    public function getGoogleFonts(){

        $fonts = [];
        if($this->getStoreConfig('basecustom/font_custom/font_family') == 'google'){
            $body_font = $this->getStoreConfig('basecustom/font_custom/google_font_family');
            $fonts[] = $body_font;
        }

        if($this->getStoreConfig('basecustom/font_custom/button_font_family') == 'google'){
            $button_font = $this->getStoreConfig('basecustom/font_custom/button_google_font_family');
            $fonts[] = $button_font;
        }

        if($this->getStoreConfig('basecustom/font_custom/menu_font_family') == 'google'){
            $menu_font = $this->getStoreConfig('basecustom/font_custom/menu_google_font_family');
            $fonts[] = $menu_font;
        }

        if($this->getStoreConfig('basecustom/font_custom/page_title_font_family') == 'google'){
            $page_title_font = $this->getStoreConfig('basecustom/font_custom/page_title_google_font_family');
            $fonts[] = $page_title_font;
        }

        if($this->getStoreConfig('basecustom/font_custom/block_title_font_family') == 'google'){
            $block_title_font = $this->getStoreConfig('basecustom/font_custom/block_title_google_font_family');
            $fonts[] = $block_title_font;
        }

        if($this->getStoreConfig('basecustom/font_custom/price_font_family') == 'google'){
            $price_font = $this->getStoreConfig('basecustom/font_custom/price_google_font_family');
            $fonts[] = $price_font;
        }

        if($this->getStoreConfig('basecustom/font_custom/product_name_font_family') == 'google'){
            $product_name_font = $this->getStoreConfig('basecustom/font_custom/product_name_google_font_family');
            $fonts[] = $product_name_font;
        }

        $fonts = array_filter($fonts);

        $links = '';

        foreach ($fonts as $_font){
            $links .= '<link href="//fonts.googleapis.com/css?family=' . $_font . ':300,400,500,600,700" rel="stylesheet" type="text/css"/>';
        }

        return $links;
    }
}
