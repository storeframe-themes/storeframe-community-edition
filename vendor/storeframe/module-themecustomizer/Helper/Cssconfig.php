<?php
namespace Storeframe\Themecustomizer\Helper;

class Cssconfig extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $_storeManager;
    protected $generatedCssFolder;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager

    )
    {
        $this->_storeManager = $storeManager;

        $this->generatedCssFolder = 'basecustom/css';

        parent::__construct($context);
    }

    public function getBaseMediaUrl(){
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
    
    public function getStoreCode(){
		return $this->_storeManager->getStore()->getCode();
	}
	
	public function getCssFilePath($storeCode = null){
        $storeCode = !is_null($storeCode) ? $storeCode : $this->getStoreCode();
		return $this->generatedCssFolder .'/style-'. $storeCode . '.css';
	}

    public function getCssFileUrl(){
        return $this->getBaseMediaUrl(). $this->generatedCssFolder .'/style-'. $this->getStoreCode() . '.css';
    }
}
