# StoreFrame Theme Customizer

This module implements easy styling overwrite (theme options) from Magento backend.
Via this module, user can easily change their ie: fonts and colors.
This module also adjust Magento product list page responsive behaviour.
