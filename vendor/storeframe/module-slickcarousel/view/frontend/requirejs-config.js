var config = {
    map: {
        '*': {
            'slick': 'Magento_PageBuilder/js/resource/slick/slick',
			'slickwrapper': 'Storeframe_SlickCarousel/js/slickwrapper'
        }
    },
	shim: {
		'slick': {
			deps: ['jquery']
		},
		'slickwrapper': {
			deps: ['jquery', 'slick']
		}
	}
};