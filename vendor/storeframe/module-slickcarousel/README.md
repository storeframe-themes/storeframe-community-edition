# StoreFrame Slick Carousel

This module implements Slick Carousel for various slider application.
Upon using this module deactivate swissup/module-slick-carousel as it would conflict with each other.
