<?php
namespace Storeframe\SlickCarousel\Model\Config\Source;

class Type implements \Magento\Framework\Option\ArrayInterface
{
	public function toOptionArray()
	{
		return [
			['value' => 'image', 'label' => __('Image')],
			['value' => 'product', 'label' => __('Product')]];
	}
}