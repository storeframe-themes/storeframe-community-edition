<?php
namespace Storeframe\SlickCarousel\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class SliderConfigurator extends Template implements BlockInterface
{
	protected $_template = "widget/slider-configurator.phtml";
}