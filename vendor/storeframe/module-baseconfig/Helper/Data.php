<?php

namespace Storeframe\Baseconfig\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_API_URL = "baseconfig/general/api_url";
    const XML_PATH_API_KEY = "baseconfig/general/api_key";

    protected $_scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig, Context $context)
    {
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function getStoreConfig($config_path)
    {
        return $this->_scopeConfig->getValue($config_path, ScopeInterface::SCOPE_STORE);
    }

    public function getAPIUrl()
    {
        $key = $this->_scopeConfig->getValue(self::XML_PATH_API_URL, ScopeInterface::SCOPE_STORE);
        return $key;
    }

    public function getAPIKey()
    {
        $key = $this->_scopeConfig->getValue(self::XML_PATH_API_KEY, ScopeInterface::SCOPE_STORE);
        return $key;
    }
}