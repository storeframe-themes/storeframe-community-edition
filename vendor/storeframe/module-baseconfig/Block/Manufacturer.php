<?php

namespace Storeframe\Baseconfig\Block;

/**
 * Class Manufacturer
 * @package Storeframe\Baseconfig\Block
 */
class Manufacturer extends \Magento\Catalog\Block\Product\View\Description
{
    protected $_attributeCollection;
    /**
     * @var \Magento\Swatches\Helper\Media
     */
    protected $_media;
    /**
     * @var \Magento\Swatches\Model\ResourceModel\Swatch\Collection
     */
    protected $_swatchCollection;
	
    protected $_moduleManager;

    /**
     * Manufacturer constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     * @param \Magento\Swatches\Helper\Media $_media
     * @param \Magento\Swatches\Model\ResourceModel\Swatch\Collection $_swatchCollection
     */
    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
                                \Magento\Framework\Registry $registry, array $data = [],
                                \Magento\Swatches\Helper\Media $_media,
                                \Magento\Swatches\Model\ResourceModel\Swatch\Collection $_swatchCollection,
								\Magento\Framework\Module\Manager $moduleManager
    )
    {
        parent::__construct($context, $registry, $data);
        $this->_media = $_media;
		$this->_moduleManager = $moduleManager;
        $this->_swatchCollection = $_swatchCollection;
    }

    /**
     * @return mixed
     */
    public function hasManufacturer()
    {
        $_product = $this->getProduct();
        return $_product->getManufacturer();
    }

    /**
     * @return mixed
     */
    public function getStoreLabel()
    {
        return $this->getProduct()->getResource()->getAttribute('manufacturer')->getStoreLabel();
    }

    /**
     * @return bool
     */
    public function isText()
    {
        return $this->getBrandValue() && empty($this->getItem()->getValue());
    }

    /**
     * @return bool|false|int
     */
    public function isColor()
    {
        if ($this->isText()) {
            return false;
        }
        //Check for a hex color string '#c1c2b4'
        return preg_match('/^#[a-f0-9]{6}$/i', $this->getItem()->getValue());
    }

    /**
     * @return mixed
     */
    public function getTextValue()
    {
        return $this->getBrandValue();
    }

    /**
     * @return mixed
     */
    public function getColorValue()
    {
        return $this->getItem()->getValue();
    }

    public function getLinkUrl()
    {
        $url = null;
        $return = true;
		if(!$this->_isEnableAttributePage()){
			return 'javascript:void(1);';
		}
		
		$attrs = $this->_getAttributeCollection();
        if ($attrs->count()) {
            $attr = $attrs->getFirstItem();
            $url = $attr->getUrl();
            if (!$attr->getData('parent_page')) { // check excluded
                $return = false;
            }
        }else{
            $return = false;
        }
        if (!$return) {
            return 'javascript:void(0);';
        }
        return $url ? $url : $this->getBrandValue();
    }

    /**
     * @return string
     */
    public function getImageValue()
    {
        return $this->_media->getSwatchAttributeImage('swatch_image', $this->getItem()->getValue());
    }

    /**
     * @return mixed
     */
    public function getBrandValue()
    {
        return $this->getProduct()->getResource()->getAttribute('manufacturer')->getFrontend()->getValue($this->getProduct());
    }

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getItem()
    {
        $brandId = $this->getProduct()->getManufacturer();
        $swatchCollection = $this->_swatchCollection;
        $swatchCollection->addFieldtoFilter('option_id', $brandId);
        return $swatchCollection->getFirstItem();
    }


    protected function _getAttributeCollection()
    {
        if (null === $this->_attributeCollection) {
            $optionId = $this->getProduct()->getManufacturer();
            $storeId = $this->_storeManager->getStore()->getId();
			//Get Object Manager Instance
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $this->_attributeCollection = $objectManager->get('Swissup\Attributepages\Model\ResourceModel\Entity\Collection')
                ->addOptionOnlyFilter()
                ->addFieldToFilter('attribute_id', ['eq' => $this->getProduct()->getResource()->getAttribute('manufacturer')->getId()])
                ->addUseForAttributePageFilter()
                ->addStoreFilter($storeId)
                ->addFieldToFilter('option_id', ['eq' => $optionId])
                ->setOrder('main_table.title', 'asc');
            // filter pages with the same urls: linked to All Store Views and current store
            $urls = $this->_attributeCollection->getColumnValues('identifier');
            $duplicateUrls = [];
            foreach (array_count_values($urls) as $url => $count) {
                if ($count > 1) {
                    $duplicateUrls[] = $url;
                }
            }
            foreach ($duplicateUrls as $url) {
                $idsToRemove = [];
                $removeFlag = false;
                $attributes = $this->_attributeCollection->getItemsByColumnValue('identifier', $url);
                foreach ($attributes as $attribute) {
                    if ($attribute->getStoreId() !== $storeId) {
                        $idsToRemove[] = $attribute->getId();
                    } else {
                        $removeFlag = true;
                    }
                }
                if ($removeFlag) {
                    foreach ($idsToRemove as $id) {
                        $this->_attributeCollection->removeItemByKey($id);
                    }
                }
            }
        }
        return $this->_attributeCollection;
    }

    protected function _isEnableAttributePage()
    {
		if(!$this->_moduleManager->isEnabled('Swissup_Attributepages')){
			return false;
		}
        $storeId = $this->_storeManager->getStore()->getId();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $c = $objectManager->get('Swissup\Attributepages\Model\ResourceModel\Entity\Collection')
            ->addAttributeOnlyFilter()
            ->addUseForAttributePageFilter()
            ->addStoreFilter($storeId)
            ->addFieldToFilter('attribute_id', ['eq' => $this->getProduct()->getResource()->getAttribute('manufacturer')->getId()])
            ->setOrder('main_table.title', 'asc');
        if ($c->count() <= 0) {
            return false;
        }

        return $c->getFirstItem()->get('use_for_attribute_page');
    }

}