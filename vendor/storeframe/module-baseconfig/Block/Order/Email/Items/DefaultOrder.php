<?php

namespace Storeframe\Baseconfig\Block\Order\Email\Items;

/**
 * Sales Order Email items default renderer
 *
 * @api
 * @author     Magento Core Team <core@magentocommerce.com>
 * @since 100.0.2
 */
class DefaultOrder extends \Magento\Sales\Block\Order\Email\Items\Order\DefaultOrder
{
    protected $_appEmulation;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_appEmulation = $objectManager->create('Magento\Store\Model\App\Emulation');
    }

    public function getProductImageUrl($product, $imageType = 'product_page_image_small')
    {
        $storeId = $this->_storeManager->getStore()->getId();

        $this->_appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);

        $imageBlock = $this->getLayout()->createBlock('Magento\Catalog\Block\Product\ListProduct');
        $productImage = $imageBlock->getImage($product, $imageType);
        $imageUrl = $productImage->getImageUrl();

        $this->_appEmulation->stopEnvironmentEmulation();

        return str_replace('/pub/', '/', $imageUrl);
    }

    public function getTemplate()
    {
        return 'Storeframe_Baseconfig::email/items/order/default.phtml';
    }
}
