<?php
namespace Storeframe\Baseconfig\Block;

/**
 * Class HolidayTime
 *
 * @package Storeframe\Baseconfig\Block
 */
class HolidayTime extends Base
{
    /**
     * Opening Time Check
     *
     * @param $today
     * @param $date
     * @return string
     */
    public function openingTimeCheck($today, $date)
    {
        $isHoliday = $this->isHoliday();
        $currentTime = date("H:i"); // Today Time
        if (!$date){
            return $today . ' day ' . 'close';
        }

        if (strpos($date,' - ') !== false) { // If Today Time doesn't contain delimiter
            list($openFrom, $openTo) = explode(" - ", $date);
            return $currentTime > $openFrom && $currentTime < $openTo && !$isHoliday ? $today . ' ' . 'open' : $today . ' ' . 'close';
        } else {
            return $today . ' ' . 'close';
        }
    }

    /**
     * is Holiday
     *
     * @return bool
     */
    protected function isHoliday() {
        if ($this->getEnableHoliday()) {
            $holidays = $this->getHoliday();
            $today = strtolower(date('d'));
            $month = strtolower(date('m'));

            if (is_array($holidays) && sizeof($holidays) > 0) {
                foreach ($holidays as $holiday) {
                    if (gettype($holiday) == "array") {
                        if ($holiday['day'] == $today && $holiday['month'] == $month)
                            return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Get Holiday
     *
     * @return mixed
     */
    public function getHoliday() {
        $storeField = $this->_scopeConfig->getValue('general/opening_holiday/holidays', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $this->serializer->unserialize($storeField);
    }

    /**
     * Get enable Holiday
     *
     * @return mixed
     */
    public function getEnableHoliday() {
        $storeField = $this->_scopeConfig->getValue('general/opening_holiday/enable_open_close_holiday', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $storeField;
    }
}
