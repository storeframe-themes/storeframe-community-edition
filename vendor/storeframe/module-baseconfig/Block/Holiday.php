<?php
namespace Storeframe\Baseconfig\Block;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;

/**
 * Class Holiday
 *
 * @package Storeframe\Baseconfig\Block
 */
class Holiday extends Base {

    /**
     * Holiday
     *
     * @return mixed
     */
    public function getHoliday() {
        $storeField = $this->_scopeConfig->getValue('general/opening_holiday/holidays', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $this->serializer->unserialize($storeField);
    }
}
