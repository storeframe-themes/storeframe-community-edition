<?php
namespace Storeframe\Baseconfig\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Base
 *
 * @package Storeframe\Baseconfig\Block
 */
class Base extends \Magento\Framework\View\Element\Template
{
    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected $serializer;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;

    /**
     * ConfigBlock constructor.
     * @param Template\Context $context
     * @param ScopeConfigInterface $config
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ScopeConfigInterface $config,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        array $data = []
    ) {
        $this->config = $config;
        $this->_countryFactory = $countryFactory;
        $this->serializer = $serializer;
        parent::__construct($context, $data);
    }

    /**
     * Store config
     *
     * @param $path
     * @return mixed
     */
    public function getStoreConfig($path)
    {
        $storeField = $this->_scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $storeField;
    }

    /**
     * get country name
     *
     * @param $countryCode
     */
    public function getCountryName($countryCode)
    {
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        echo $country->getName();
    }
}