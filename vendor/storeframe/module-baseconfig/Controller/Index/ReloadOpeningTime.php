<?php

namespace Storeframe\Baseconfig\Controller\Index;

use Magento\Framework\App\Action\Action;

class ReloadOpeningTime extends Action
{
    protected $_helperData;
    protected $resultJsonFactory;

    public function __construct(
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\App\Action\Context $context
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();

        $blockInstance = $this->_objectManager->get('Storeframe\Baseconfig\Block\HolidayTime');
        
        date_default_timezone_set($blockInstance->getStoreConfig('general/locale/timezone')); // Timezone
	    $today = strtolower(date('l')); // Today Day
	    $data = $blockInstance->getStoreConfig('general/opening_time/time_'.$today );
	    
        $res = $blockInstance->openingTimeCheck($today, $data);
    
        return $resultJson->setData(['result' => $res]);
    }
}