<?php

namespace Storeframe\Baseconfig\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{

    /**
     * Customer setup factory
     *
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $_eavSetupFactory;

    /**
     * Init
     *
     * @param \Magento\Eav\Setup\EavSetupFactory
     * CategorySetupFactory categorySetupFactory
     */
    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory) {
        $this->_eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Installs DB schema for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->_eavSetupFactory->create([
            'setup' => $setup
        ]);

        if ($context->getVersion() < '1.0.5') {
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'sf_additional_details',
                [
                    'group' => 'Content',
                    'sort_order' => 101,
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => '*Additional Details',
                    'input' => 'textarea',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'wysiwyg_enabled' => true,
                    'is_html_allowed_on_front' => true,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false
                ]
            );
        }
        
        if ($context->getVersion() < '1.0.6') {
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'sf_plp_description',
                [
                    'type' => 'text',
                    'label' => '*Product List Page Description',
                    'required' => false,
                    'searchable' => false,
                    'filterable' => false,
                    'is_use_define' => true,
                    'class' => '',
                    'group' => 'Content',
                    'input' => 'textarea',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'visible' => true,
                    'user_defined' => false,
                    'default' => '',
                    'sort_order' => 102,
                    'apply_to' => '',
                    'note' => '',
                    'used_in_product_listing' => true
                ]
            );
        }
	
		if ($context->getVersion() < '1.0.7') {
			$eavSetup->updateAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'sf_additional_details',
				[
					'attribute_code' => 'stf_additional_details'
				]
			);
		}
	
		if ($context->getVersion() < '1.0.8') {
			$eavSetup->updateAttribute(
				\Magento\Catalog\Model\Product::ENTITY,
				'sf_plp_description',
				[
					'attribute_code' => 'stf_plp_description'
				]
			);
		}
        // add manufacture to default attribute set
        if ($context->getVersion() < '1.0.9') {

            $attributeSetName = 'Default';
            $groupName = 'General';
            $attributeCode = 'manufacturer';

            //-------------- add attribute to set and group
            $attributeSetId = $eavSetup->getAttributeSetId('catalog_product', $attributeSetName);
            $attributeGroupId = $eavSetup->getAttributeGroupId('catalog_product', $attributeSetId, $groupName);
            $attributeId = $eavSetup->getAttributeId('catalog_product', $attributeCode);

            $eavSetup->addAttributeToSet($entityTypeId = 'catalog_product', $attributeSetId, $attributeGroupId, $attributeId, 7);
        }
    }
}