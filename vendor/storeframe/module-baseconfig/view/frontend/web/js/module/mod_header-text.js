var firstTime = true;
define([
	'jquery',
	'Magento_Ui/js/modal/modal',
	'mage/url'
], function ($, modal, MageUrl) {
	$('.js-modal-customer-service').click(function() {
		$('.customer-service-popup').modal('openModal');
	});
	
	setTimeout(function(){
		if ( window.innerWidth < 767 ){
			$('.footer.copyright').addClass('on__mobile');
		}
		
		if ( $('#notice-cookie-block > .content').outerHeight() == 0 ){
			$('.footer.copyright').removeClass('on__mobile');
		}
	}, 900);
	
	$('#btn-cookie-allow').click(function(){
		$('.footer.copyright').removeClass('on__mobile');
	});
	
	$(function() {
		var url = MageUrl.build('/base_config/index/ReloadOpeningTime');
		
		if(!firstTime){
			return;
		}
		
		$.ajax({
			url: url,
			type: "GET",
			data: {},
			success: function(response){
				if(response.result != ''){
					$('.js-stf-openning-time').addClass('webshop-status ' + response.result);
					firstTime = false;
				}
			}
		});
	});
});
