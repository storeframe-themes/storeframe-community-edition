define([
	'jquery',
	'mage/translate'
], function ($, $t) {
	'use strict';
	
	return function (config, node) {
		var moreLess = {
			button: {
				el: $("<a>", {
					class: "toggle-moreless action",
					href: "#"
				}),
				expanded_text: $.mage.__('Show less'),
				collapsed_text: $.mage.__('Show more')
			},
			target: {
				el: $(node),
				height: $(node).height(),
				maxHeight: config.contentMaxHeight,
				collapsedClassName: "collapsed",
				defaultClassName: "js-more-less",
			}
		};
		
		if (moreLess.target.height > moreLess.target.maxHeight) {
			// update button text value
			moreLess.button.el.text(moreLess.button.collapsed_text);
			
			moreLess.target.el
			// add css class to apply some styling
			.addClass(moreLess.target.collapsedClassName)
			.addClass(moreLess.target.defaultClassName)
			// append link to product description
			.parent().find(moreLess.target.el).after(moreLess.button.el);
			moreLess.target.el.css('max-height', moreLess.target.maxHeight);
		}
		
		moreLess.button.el.on("click", function (e) {
			e.preventDefault();
			
			if (moreLess.target.el.hasClass(moreLess.target.collapsedClassName)) {
				moreLess.target.el.removeClass(moreLess.target.collapsedClassName);
				moreLess.button.el.text(moreLess.button.expanded_text);
				moreLess.target.el.removeAttr("style");
			} else {
				moreLess.target.el.addClass(moreLess.target.collapsedClassName);
				moreLess.button.el.text(moreLess.button.collapsed_text);
				moreLess.target.el.css('max-height', moreLess.target.maxHeight);
			}
		});
		
		$(function(){
			$('.js-tab-click').click(function(){
				$('html, body').animate({
					scrollTop: ($($(this).find('a').attr('data-anchor')).offset().top - $('.product-info-sticky').outerHeight() - 10)
				}, 300);
			});
		});
	}
});