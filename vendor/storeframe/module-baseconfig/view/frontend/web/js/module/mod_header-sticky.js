define([
	'jquery'
], function ($) {
	$(function() {
		// Sticky Header (Only when scroll up & not in PDP)
		if (!$('body').hasClass("catalog-product-view")) {
			var didScroll;
			var lastScrollTop = 0;
			var delta = 5;
			var navbarHeight = $('.is-header-sticky').outerHeight();
			
			$('body').css('padding-top', navbarHeight);
			$('.is-header-sticky').css('height', navbarHeight);
			$('.nav-up').css('top', -navbarHeight);
			
			$(window).scroll(function(event){
				didScroll = true;
			});
			
			setInterval(function() {
				if (didScroll) {
					hasScrolled();
					didScroll = false;
				}
			}, 250);
			
			function hasScrolled() {
				var st = $(this).scrollTop();
				
				// Make sure they scroll more than delta
				if(Math.abs(lastScrollTop - st) <= delta)
					return;
				
				// If they scrolled down and are past the navbar, add class .nav-up.
				// This is necessary so you never see what is "behind" the navbar.
				if (st > lastScrollTop && st > navbarHeight){
					// Scroll Down
					$('.is-header-sticky').removeClass('nav-down').addClass('nav-up');
					$('.is-header-sticky').css('top', -navbarHeight);
				} else {
					// Scroll Up
					if(st + $(window).height() < $(document).height()) {
						$('.is-header-sticky').addClass('nav-down').removeClass('nav-up');
						$('.is-header-sticky').css('top', '');
					}
				}
				
				lastScrollTop = st;
			}
		} else {
			$('.header-collected-space').removeClass('is-header-sticky');
		}
	});
});
