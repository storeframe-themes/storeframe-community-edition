<?php
namespace Storeframe\Baseconfig\Model\Config\Source;

class ButtonAction implements \Magento\Framework\Option\ArrayInterface
{
	public function toOptionArray()
	{
		return [
			['value' => 'addtocart', 'label' => __('Add to Cart')],
			['value' => 'viewproduct', 'label' => __('View Product')],
			['value' => 'showboth', 'label' => __('Display Both')],
			['value' => 'noneremoved', 'label' => __('None / Removed')],
			];
	}
}
