<?php

namespace Storeframe\Baseconfig\Cron;

use Storeframe\Baseconfig\Helper\Data;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\HTTP\Client\Curl;

class DomainVerify
{
    protected $_helperData;
    protected $_storeManager;
    protected $_curl;

    public function __construct(Data $helper, StoreManagerInterface $storeManager, Curl $curl)
    {
        $this->_helperData = $helper;
        $this->_storeManager = $storeManager;
        $this->_curl = $curl;
    }

    public function execute()
    {
        $url = $this->_helperData->getAPIUrl();
        $apiKey = $this->_helperData->getAPIKey();
        $websites = $this->_storeManager->getWebsites();
        $domains = [];

        foreach ($websites as $website) {
            foreach ($website->getStores() as $store) {
                $storeObj = $this->_storeManager->getStore($store);
                $domains[] = $storeObj->getBaseUrl(UrlInterface::URL_TYPE_WEB);
            }
        }

        $data = array(
            'password' => $apiKey,
            'domain' => $domains,
        );

        $this->_curl->post($url, $data);
    }
}
