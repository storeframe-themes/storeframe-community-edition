<?php

namespace Experius\AddressLines\Model\Validator\Attribute;

use Magento\Eav\Model\Attribute;

class Data extends \Magento\Eav\Model\Validator\Attribute\Data
{
    protected $_needFixNullValueAttributes = ['house', 'company'];
    protected $_fixedNullAttributeValue = '0';

    /**
     * Validate EAV model attributes with data models
     *
     * @param \Magento\Framework\Model\AbstractModel $entity
     * @return bool
     */
    public function isValid($entity)
    {
        /** @var $attributes Attribute[] */
        $attributes = $this->_getAttributes($entity);

        $data = [];
        if ($this->_data) {
            $data = $this->_data;
        } elseif ($entity instanceof \Magento\Framework\DataObject) {
            $data = $entity->getData();
        }

        foreach ($attributes as $attribute) {
            $attributeCode = $attribute->getAttributeCode();
            if (!$attribute->getDataModel() && !$attribute->getFrontendInput()) {
                continue;
            }
            $dataModel = $this->_attrDataFactory->create($attribute, $entity);

            $dataModel->setExtractedData($data);
            $fixValue = null;
            if (in_array($attributeCode, $this->_needFixNullValueAttributes)) {
                $fixValue = $this->_fixedNullAttributeValue;
            }
            if (!isset($data[$attributeCode])) {
                $data[$attributeCode] = $fixValue;
            }
            $result = $dataModel->validateValue($data[$attributeCode]);
            if (true !== $result) {
                $this->_addErrorMessages($attributeCode, (array)$result);
            }
        }
        return count($this->_messages) == 0;
    }

}
